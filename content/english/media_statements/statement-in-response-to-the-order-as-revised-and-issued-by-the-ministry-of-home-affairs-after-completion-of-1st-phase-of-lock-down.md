+++
bg_image = "/images/modi-assocham-5-e1585116677933.jpeg"
date = 2020-04-16T18:30:00Z
description = "The Hon’ble Prime Minister’s nationwide address on the lockdown extension, did not emphasise any concrete policy measures needed to urgently support migrant workers. . While we wholeheartedly support the government in its fight against the COVID-19, we release this statement with a great sense of urgency and anguish, reiterating the large scale, ongoing humanitarian crisis that has simultaneously unfolded and has escalated in the country."
image = "/images/modi-assocham-5-e1585116677933.jpeg"
tags = []
title = "Statement in response to the order as revised and issued by the Ministry of Home Affairs after completion of 1st Phase of Lock-down"
type = ""

+++
**We, the Working People’s Charter & Aajeevika Bureau, would like to hereby release our joint statement in response to the order as revised and issued by the Ministry of Home Affairs, that the lockdown measures, stipulated in consolidated guidelines of the Ministry of Health & Family Welfare for the containment of the COVID 19, be extended for a further 20 days till the 3rd of May.**

The Hon’ble Prime Minister’s nationwide address on the lockdown extension, did not emphasise any concrete policy measures needed to urgently support migrant workers. While we wholeheartedly support the government in its fight against the COVID-19, we release this statement with a great sense of urgency and anguish, reiterating the large scale, ongoing humanitarian crisis that has simultaneously unfolded and has escalated in the country.

Over the last 20 days India’s migrant workers, who make up close to 30 % of our workforce, have been severely affected by this lockdown. They are stranded in our cities, cities that they build for us, with the sweat of their labour. Right now, with everything shut, millions of them have no work and no food. Many have no shelter, having been evicted by their landlords. Many organizations and sections of the media have been repeatedly working relentlessly providing updates on this situation on the ground. We believe it is high time that the central and state governments respond to this issue a whole lot better than they have thus far.

Provided that the lockdown period has been extended as per this order, we demand that the government immediately take the following measures:

Make PDS Universal: We would like to reiterate with greater urgency than ever before that food grain under the PDS system must be made universal. There are approximately 108.4 million people who are excluded from the PDS; that is 4 times the population of Australia, and approximately 7 and a half times the size of an average European country. To put things further into perspective, there are more people in need and outside the PDS system, than there are people in 182 out of 195 countries in the world. There is every indication therefore to immediately make PDS universal and to release food stocks from the Food Corporation of India. Cabinet Minister for Consumer Affairs & Public Distribution, Shri Ramvilas Paswan has said the government is already burdened with 46000 crores of subsidy provided under the NFSA. Our response to this claim is twofold. First this is less than 0.5% of India’s GDP, and therefore not near sufficient in the actual money that needs to be allocated towards food subsidies. Second, this does not change the fact that universalizing PDS is the only way that food can reach a large number of people. As long as access to food grains under the PDS is contingent on ration cards, no matter how much of food stock is released by the Food Corporation of India, it will remain inaccessible to the 108.4 million people mentioned above.

\- Clear dues under MGNREGA: The order in section 10 states that ‘MGNREGA works shall be allowed’ adding further in sub-section (ii) that ‘priority be given to irrigation and water conservation’. We would like to reiterate with utmost urgency that 1800 crores remain pending in the form of arrears under the MGNREGA. This is money that rightfully belongs to the people and must be released immediately before new work under this act is initiated.

Additionally we strongly suggest the advance payment of next 3 moths of work, i.e 90 days, the work allocation can be made once the lockdown period gets over.

\- Address Wage retrenchments and non-payment of wages immediately: have severely hampered a large segment of this population. Previously, the government has asserted that because food and shelter have been sufficiently provided, wage compensation need not be carried out with immediate effect. Such an argument equates and reduces wages to subsistence as if, the migrant, works and earns only to live. We firmly assert that these workers be paid what is rightfully theirs, and the government immediately set up mechanisms to ensure the same. While the ministry of Labour and Employment has set up control rooms to address grievances relating to wages, we believe this will not help resolve the problem. The Government must engage workers collectives around the country and prepare the list of vulnerable workers to be provided with at least 2 months of minimum wages of Rs.375/day as proposed by the Anoop Satpathy led expert committee.

\- Prioritize subsidies to specific industries: We are also mindful of the fact that small and medium scale industries are the backbone of a country's export trade, however the industry does not have enough capital to provide wages to workers. The textile industry, for instance, etc needs to be and considered for subsidies on a priority basis without which the industry is likely to collapse. We demand that the government must calculate the losses and provide appropriate financial support to MSMEs.

\- Arrange for transport in coordination with state governments in ensuring that migrant workers are allowed to go home: Several resources were exhausted to bring back Indian citizens abroad. We fail to understand why the government hesitates to make similar arrangements for the millions of workers who are stranded within Indian cities without work, and incomes, who wish to go home. We demand that the government takes this issue seriously so as to ensure a safety passage for those wanting to go back.

We would like to reiterate; that this is a public health emergency of international concern, and we wholeheartedly acknowledge and extend our support to measures taken by the government of India in safeguarding its people. However, the fact of the matter is that the nationwide lockdown has led to a humanitarian crisis, which can, if left unchecked, erode into a human tragedy of enormous proportions. We would like to clarify in no ambiguous terms that this is a matter of life and death, for millions of Indians. Everyone citizen must stand shoulder to shoulder, with the government in protecting India’s most vulnerable citizens.

We would like to assure the government our unconditional support and in every which way. We are hopeful that in full cognizance of the urgency of our statement, the government will with immediate effect address all of our aforementioned demands.

Sincerely

Working Peoples’ Charter

Aajeevika Bureau

For more information please contact

Chandan Kumar - +91 9717891696 workerscharterprocess@gmail.com

Divya Varma - +91 9606359333 divya.varma@aajeevika.org