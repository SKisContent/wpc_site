+++
bg_image = "/images/1129018.jpeg"
date = 2022-02-16T06:30:00Z
description = "**_Highlights non-compliance of labour laws; negligence by the builder, contractors, and authorities; long working hours; inadequate compensation and rehabilitation provisions for the injured_**"
image = "/images/whatsapp-image-2022-02-11-at-18-33-39.jpeg"
tags = []
title = "Independent Fact-Finding team releases its interim report on the death of the 5 workers at under construction Bluegrass Business Park, Yerawada-Pune"
type = ""

+++
**_Highlights non-compliance of labour laws; negligence by the builder, contractors, and authorities; long working hours; inadequate compensation and rehabilitation provisions for the injured_**

Mumbai-Pune, 16th February 2022: An independent team of experts was constituted immediately to investigate the death of five construction workers due to the collapse of a giant slab in an under-construction site in Pune’s Yerawada area late in the night of the 3rd of February. The team visited the Bluegrass Park- the site of the incident, on 5th February. The independent team had met with all five injured workers at the hospital, it also went and met with the government officials and local organisations. In its interim finding, the independent team has highlighted he non-compliance of about 8 labour laws, negligence by the builder, the contractor and the authorities, long working hours and has demanded a thorough investigation by including the labour representatives and adequate compensation to the injured.

One of the shocking findings of the team was the inadequate or no provision for the safety and security arrangements for the workers on the site. The team observed that “there was neither a first-aid kit available at the site nor an MFR (Medical First Response) kit available with the responding agencies e.g., Neck Collars / Stiff Neck collars, Splints, Masks etc.” This team also recorded that there was a delay in the rescue operation and inadequate arrangements were made as a result “victims were piled into the ambulance along with able-bodied survivors to accompany the injured.”

As these are interim findings, the team also intend to investigate the ownership of the land where the construction is taking place. The report points out that “the land where the development is taking place is "Maharvatan" land and was leased to Mr Wadia and 2 others for 99 years and the lease expired in 1995”.

The team found it unfair to not have included the workers' representatives in the investigation committee constituted by the Pune Municipal Corporation and has demanded that “a committee of citizens should be formed immediately consisting of representatives of worker organisations, PMC representatives, Labour Commissioner’s office and a reputed engineer and architect employers.

It also highlighted another important aspect from the mental health point of view which often gets neglected in such incidences. The interim report notes that “Those who escaped or survived have been left traumatised, with significant physical and psychological scarring”. The report further adds that “Such memories will cause short- and long-term mental disorders and Post Traumatic Stress Disorders ranging from e.g., nightmares, fear of loud sounds, stigma, frustration to suicidal ideations. Lack of / inadequate compensation and support will only compound these mental health issues.” In addition to this, due to their weak economic background and difficulty in accessing adequate healthcare and rehabilitation services, their quality of life will further deteriorate.

**FAILURE ON THE PART OF THE OFFICE OF LABOUR COMMISSIONER TO IMPLEMENT THE LABOUR LAWS**

The slab collapse at the construction site in Yerawada, Pune on 3rd February which led to the death of five labourers could have been prevented by due diligence by the office of Labour Commissioner. Safety inspections and a simple check on compliance of labour laws would have revealed in time several illegal lapses in securing the health and safety rights of the labourers. The workers were not registered under the Building and Other Construction Workers (BOCW) Act, 1996 and other safety measures provided under the act such as the provision of protective gear, the appointment of safety officer etc were not followed. The workers, who were migrants from Bihar, were also not registered with the Maharashtra Building and Other Construction Workers Welfare Board or the Interstate Migrant Workers Act, 1979. The Officer of Labour Commissioner is responsible for the implementation of these acts. However, none of the welfare and safety measures was implemented in this case. The Labour Commissioner has admitted that the labour laws were not being implemented.

Before the incident, no safety inspection was carried out at the construction site by the Industries, Energy and Labour Department and no visits to the site were undertaken due to a policy of the government which requires that all complaints regarding unsafe construction practices must be made online first, only then the officers of the Labour Department can be deployed for an inspection under the “Central Inspection System.” This policy/circular needs to be done away with and regular monitoring of safety requirements need to be carried out under the supervision of the labour department.

An FIR was later registered under section 304 (culpable homicide not amounting to murder), 336 (act endangering life and personal safety of others) against four sub-contractors; however, the principal employer, Mr Farrokh Framji Wadia was not named as an accused. Under the BOCW Act, the Principal Employer is responsible for ensuring minimum conditions of work at the construction sites - including ranges of provisions related to wages, working hours, maintenance of registers and records, and safety in work conditions. Despite the failure in ensuring the safety of labourers, neither Wadai nor the main contractor, Mr Mohan Achalkar, of Ahluwalia Contracts India Private Limited was arrested and no other action has been taken against them.

The incident is the fourth such incident in Pune city where workers, especially migrant workers, have lost their lives in construction work, indicating a pattern of negligence by the Labour Commissioner and employers towards the health and safety of labourers. These workers were from an economically backward area in Bihar and were forced to migrate due to the unavailability of employment.

_The Independent fact-finding team was comprised of Adv Gayatri Singh, sr. counsel Bombay High Court; Adv Mohan Waradkar, Dr Shaila Ansari, Public health expert; Chandan Kumar, Labour Expert; Kashinath Nakathe and Bilal Khan, from trade unions; Shweta Damle, HALWA; Raju Vanjare, YUVA; Deepak Paradkar, Ajeevika Bureau; Mehboob Nadaf, local representative_

**For details contact:**

Chandan Kumar 9177891696

Kashinath Nakathe 9922684989

**Click here to Download Marathi and English Press Release:**

[English Press Release](https://drive.google.com/file/d/1CN88GnStS601e6DD04LEBSgZjAz0xS-4/view?usp=sharing)

[Marathi Press Release](https://drive.google.com/file/d/1kgERRy-PqWFLlRfPpxtyjMQbBgaFwtxw/view?usp=sharing)

[Interim Fact Finding Report](https://drive.google.com/file/d/1_Y1_jyBQTrwFAnzLBbbyJ4K8ezkbZSmf/view?usp=sharing)