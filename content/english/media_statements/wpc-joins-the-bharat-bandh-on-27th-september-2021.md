+++
bg_image = "/images/farmer.jpg"
date = 2021-09-26T18:30:00Z
description = "###### MAZDOOR KISAN EKTA ZINDABAD!"
image = "/images/farmer.jpg"
tags = []
title = "WPC JOINS THE BHARAT BANDH ON 27TH SEPTEMBER 2021!"
type = ""

+++
**Working People’s Charter**

We the undersigned members of the WPC, hereby join the call for Bharat Band on 27 September 2021 in full solidarity with the millions of farmers of India protesting the draconian pro-corporate, anti-people ‘farm laws’ of the Union Government!

The WPC calls upon its constituents, to build the widest coalition for strengthening the ongoing farmers’ movement for protecting the dignity and livelihoods of all working people of India!

The unflinching forward march of the farmers of India has exposed the Union government’s true intent behind passing the Farmers' Produce Trade and Commerce (Promotion and Facilitation) Act, 2020, the Farmers (Empowerment and Protection) Agreement on Price Assurance and Farm Services Act, 2020 & the Essential Commodities (Amendment) Act, 2020 (together, the ‘farm laws’)! We all know that the real motive of the government is to break the back of farming communities through dismantling public-funded support institutions and facilitating the encroachment of big capital on the land and labour of rural India. The plan is simply to make agriculture unviable for farmers and push millions more out of agriculture and into poverty and debt traps for the big corporates to prey upon!

Most India’s workers have tried to organize against the authoritarian diktats of the government. Millions of farmers and workers on the streets of India since the winter of 2017 have shown the government that this daylight robbery of farmers’ lands and rights will be resisted at all costs! All the government’s attacks on the farmers and workers’ organizations in this time has failed to break the historic farmer-worker unity.

On 27 September 2021, as the Bharat Band call of the farmers shuts down India in protest, the country will witness the unity of India’s working classes against anti-democratic, anti-constitutional forces bent on selling off the country’s resources and pride to the highest bidding corporates!

The Working People’s Charter is the voice of millions of informal workers with deep roots in the villages and farms of India. We call upon our constituents to stand shoulder to shoulder with our farmer brothers and sisters in the Bharat Band of 27 September 2021 to inflict a resounding defeat to the flag-bearers of private profit and public loot!

**WPC Secretariat**

**For more details contact: Chandan Kumar (National Coordinator) - 97178 91696**