+++
bg_image = "/images/nc_wpc-height-500.png"
date = 2021-09-09T18:30:00Z
description = "The Executive Committee of the WPC network has decided to hold the National Conference of the network on 23 rd and 24th October 2021. It will start on 23rd morning at 10 am and end at 6 pm on the 24th in Hyderabad, Telangana. The National Conference will discuss the way forward for the WPC network, the structure, strategy, the vision and mission of the conference. The national office bearers will be elected."
image = "/images/nc_wpc-height-500.png"
tags = []
title = "DATES OF THE WPC NATIONAL CONFERENCE ARE CONFIRMED: 23RD AND 24TH OCTOBER, IN HYDERABAD"
type = ""

+++
The Executive Committee of the WPC network has decided to hold the National Conference of the network on 23rd and 24th October 2021. **It will start on 23 rd morning at 10 am and end at 6 pm on the 24th.** Please mark the dates. **The venue is Hyderabad, Telangana.**

The National Conference will discuss the way forward for the WPC network, the structure, strategy the vision and mission of the conference. The national office bearers will be elected.

The conference will be restricted in terms of numbers (no more than 200 delegates), and proper COVID protocols will be followed. Simple lodging and boarding for the official delegates and invitees will be taken care of by the network. Travel including local travel will be the responsibility of the state chapters and delegates themselves. A mobilising committee has been formed, which discussed the number of participants who can attend from each state, and associated organizations, national federations, etc.

**Due to limited resources and Covid issues, the quota for delegates is as follows:**

* State, where WPC state chapter is functional, will be 5, and where state chapter is under process, will be up to 2. However bigger states like UP, Maharashtra can increase this number up to 7.
* For other networks, think tanks, federations etc. the quota will be up to 2 delegates.
* State delegate quotas are excluding of EC members.
* There was a proposal to support delegates belonging to small organisations, coming from far-flung regions subject to the availability of resources.
* We have to finalize the list of delegations belonging to all quotas in the next 72 hours, i.e 16th Sept Night.

However, we encourage people who can take care of their accommodation and food, the same can be done in form of a donation to the host organisation in Hyderabad. To make sure that there is the broadest possible participation of all members –organisations and associate members of the network, online and physical state chapter meetings will be held to prepare for the conference. Documents and resolutions will be circulated soon. The meeting will discuss and decide on the mission, vision, future plans of the network.

Those from fraternal organisations, labour unions, social movements, sectoral organisations who may not be part of any of the official delegations, but who wish to attend the conference and can organize their own accommodation, please write to the secretariat in order to register. We welcome you to attend the conference. Please email us.

More information on the conference will be sent out soon.

WPC HQ: Chinmayi Naik, Chandan Kumar, Meena Menon

Hyderabad centre: Bro Varghese, Sheik Salauddin, Lissy Joseph, Sashi Kumar

**Contact numbers:**

WPC Mumbai HQ: (Chinmayi Naik): 020-40129763, +91-8884319159

Hyderabad Office: +91-8328615571, +91-8790132823

**Please download the circular here:**

[WPC Circular-National Conference 2021](https://workingpeoplescharter.in/documents/54/WPC_Circular_-_National_Conference_202https://drive.google.com/file/d/1zcEBjmcGSZ2YrP4MxQxEfy6n2kLY0Hbo/view?usp=sharing1_IFWFDUJ.pdf)