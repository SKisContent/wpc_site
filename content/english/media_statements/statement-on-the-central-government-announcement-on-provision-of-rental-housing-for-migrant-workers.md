+++
bg_image = "/images/kalyanpuri_settlements_caught_fire-height-500.jpg"
date = 2020-06-23T18:30:00Z
description = "###### On May 14th, the indian Finance Minister Nirmala Sitharaman announced a second tranche of ₹ 20 lakh crore economic stimulus which, she declared, is aimed at helping migrant labourers affected by COVID-19. Workers organisations have been working on housing for the poor for decades especially in the metros and urban areas. Struggles against eviction, for better housing, for alternate policies, for decent habitat, have very clear demands – both short term and long term. India has had the dubious distinction of being the site of some of the largest, slums in Asia. The living conditions of the urban poor in India has always been a blot on India economic success story. Many policies and schemes later, we are no closer to seeing any real change."
image = "/images/kalyanpuri_settlements_caught_fire-height-500.jpg"
tags = []
title = "STATEMENT ON THE CENTRAL GOVERNMENT ANNOUNCEMENT ON PROVISION OF RENTAL HOUSING FOR MIGRANT WORKERS."
type = ""

+++
#### On May 14th, the Finance Minister Nirmala Sitharaman announced a second tranche of ₹ 20 lakh crore economic stimulus which, she declared, is aimed at helping migrant labourers affected by COVID-19.

As part of this package, the Central government has said that it will:

* provide affordable rental houses to migrant workers and urban poor under Pradhan Mantri Awas Yojana (PMAY) scheme.
* that they will convert government funded housing in cities into affordable rental housing complexes under PPP model.
* they will encourage central government agencies and state government organisations to develop housing complexes.
* they plan to incentivize manufacturing units, industries and institutions to develop affordable housing complexes on their private lands.

Other housing related steps that have been announced include loan subsidies under a Credit Linked Subsidy Scheme under PMAY. Under this, the government will give a Rs 70,000 crore boost to the housing sector through extension of CLSS Credit Linked Subsidy Scheme. But this is restricted to benefit the middle income group (annual Income: Rs 6-18 lakh).

The GOI has not yet announced any guidelines on how they plan to operationalize the policy pronouncements. However, the package provides an opportunity for workers organisations to put forward some of the demands they have had on the way housing is conceptualized in the policy space, analysis of what policies and projects are in place from before and what needs to be done, both in the long term and the short term.

Workers organisations have been working on housing for the poor for decades especially in the metros and urban areas. Struggles against eviction, for better housing, for alternate policies, for decent habitat, have very clear demands – both short term and long term. India has had the dubious distinction of being the site of some of the largest, slums in Asia. The living conditions of the urban poor in India has always been a blot on India economic success story. Many policies and schemes later, we are no closer to seeing any real change. In fact things have changed for the worse, from the times when the public sector in post-independence India, ensured housing for employees, and so did the private sector. Today employers have no responsibility whatsoever towards employees, leading to the kind of mass marches of misery we saw during the COVID lockdown, with millions of people just left to die or walk hundreds of kilometers when construction sites, factories, shops and establishments shut down in March.

**Immediate measures: Upgrading/retrofitting built houses:**

The immediate step in the current situation is to use the funds to upgrade slums and workers housing everywhere. This is also logical if measures to tackle the health emergency is are to be effective. If the government is serious about ‘flattening the curve’ the issue of over crowding and the inadequacy of self-built housing have to be addressed. Secondly, the CLSS scheme under PMAY, should be extended to low income and economically weaker section. The working poor who live majorly in a diverse range of informal tenures are excluded from housing loans for house improvements because housing finance institutions generally require adequate documentation regarding proof of landownership. PMAY must include a programme that facilitates the extension of loans for housing improvements in conditions of informal tenures by relaxing such stringent landownership requirements, and wherein PMAY can then give an interest subsidy on such a loan. The Beneficiary led individual house construction and enhancement (BLC) requires very stringent land ownership documentation. There is evidence very weak and diverse forms of documentation especially so for the poor making them inelligible for programs like these. Investing in understanding diverse land arrangements in urban areas and practising felxibility for land ownership domentation will enable the scheme to be more inclusive of urban poor.

Upgradation presupposes a level of security in terms of tenure. When tenure rights are insecure people have reduced incentives to invest in and upgrade land and housing; they may be forced to leave a family member home to guard property rather than work or go to school; and, they may often lack access to services requiring them to devote significant portions of their time and income to acquire these. Security of tenure therefore needs to be guaranteed. Slum Upgradation should facilitate realignment of slums for provisioning of neighbourhood amenities and services.

Upgradation projects can provide livelihood opportunities in infrastructure development such as, road, water, sanitation and drainage. States like Kerala are using this opportunity to offer urban livelihood for 90 days to those who are involved in upgrading informal settlements. Similarly, Himachal Pradesh and Orissa has used urban employment guarantee scheme for 100 days to upgrade sanitation and other building services.

Upgrading existing settlements means issue of finding new land is eliminated for the time being. The only challenge is to make such land legally available for use. Whether the land is publicly or privately owned, it will require an enabling national policy framework that prevents eviction and protects communal rights and land tenure. This will cover a large number of the slums and urban poor.

But this will not be a solution for those slums which are encroached on environmental or public spaces such as bio-diversity park, grounds, reserved public spaces, etc, however, will need strategic solutions with proper local area plans to allocate viable and affordable lands within the city.

In cases of very dense slums a scheme for redevelopment through beneficiary led contribution developing walk-up tenements (G +4) along with land reservation under occupation of slums will enable create additional tenements to accommodate the ones living on precarious lands. Caution on a reasonable payment plan for beneficiary contribution would be a great enabler of inclusive development of housing for the poor.

In the big metros, very dense slums may need to be redeveloped, since the density itself will be an impediment to upgradation. Access to land for viable and affordable housing can be done through reservation of land occupied by slums for low cost housing development. Reservation of land should be only for tenable lands.

**Rental housing:**

While ownership/tenure is important to secure stability for slum dwellers, to secure the prevention of eviction from where they are, other solutions such as rental housing have to be developed side by side in order to solve the inadequacy of affordable housing especially in the metros and larger cities. This is not just an issue of workers housing, even large sections of the middle classes look for rental housing in the cities for temporary stays for study, work, experience. Therefore need to diversify the typologies of housing in more imaginative forms of working people’s hostel, dormatories and other forms of tenements.

An emphasis on ownership and ownership based housing is a market solution which is not just self-defeating in the long run, it is also not the universal need. It is not a priority for new migrants, and those who work in sectors like construction, who need to keep moving. Mass scale rental housing, initiated or built with the cooperation of state and para statal bodies, are extremely important. There are hardly any schemes to address this just now. Some states like Kerala have started working on this and built some rental units, built and maintained by a body set up for the purpose. These are solutions that can be tailored to/replicated elsewhere in the country.

Rental solutions have been an anathema as far as real estate big business is concerned. The myth that it is better to pay EMI than rent has always been peddled by the industry to make sure that the housing market stays as profitable. It also ignores the option of rental housing. The crisis in the real estate sector with huge numbers of vacant housing shows that the skewed priorities of the sector as well as the governments have to change. Ownership as core of housing solutions favours the market and privatization as solutions, where alternatives have to focus instead on public housing and social housing schemes.

**Vacant houses:**

In this context vast numbers of vacant housing which investors are holding on to because the market is not ‘favorable’, or EWS housing which people have not moved into because the construction is not decent and requires and repairs and few cases upgrading, are examples of is wasted resources which can be leased by the state for providing rental housing. These typologis of housing should also be connected with affrodable transport. The state developed housing sites are largely the rehabilitation sites that are often in unlivable conditions in the metropolitian cities. In fact, these are hotbeds for communicable disease because of its layout designs and gross violation of Building Construction Norms.

Builders keeping houses vacant should be asked to pay a tax when they keep houses vacant and refuse to sell for less. This is an issue mainly in the metros, where housing inadequacy is most critical. A scheme to turn these into rental housing is well worth considering. There is no reason why the state cannot set up bodies to undertake the management and maintenance of these projects on a PPP model. The idea of long term and short term lease needs to be explored.

**Land:**

It is by now a known fact, that no affordable housing, and that includes rental housing is possible without addressing the land question. Governments, past and present, do not want to put this on record because land is at the heart of the push for privatization. Land allocation exclusively for affordable housing should constitute an important element of any housing scheme in proportion with incremental need of affordable housing for the coming 20 years.

Access to land is necessary condition to provide housing. Land is also critical to improving access to economic opportunity, including livelihoods, credit markets, public and municipal services. Reservation of all slum land for housing for the poor must be made manadatory unless it is in spaces which are reserved for environmental purposes. Land allocation and planning are critical not just for provision of housing but to make cities more liveable, sustainable and healthy, with access to services.

**Planning:**

Indian cities are a clear example of the need for proper planning, land allocation and reservation and zoning. While re-planning older cities may be difficult smaller cities offer the opportunity to be imagined differently. Planning for people and by people should be at the core of this exercise. Ecological sensitivity and smart regional planning must also be advocated for such that industries, amenities and people are spread across a region in clusters so that there is less pressure on transport, and everything is within manageable distance. A differential approach to different sites is very important, including the rules regarding use of space and density.

**Long term solutions: decent housing:**

While making sure of what exists and improving upon it, the issue of providing decent housing for all as a long-term goal, cannot be forgotten. Current slum housing conditions are not a reflection of a the desirable option, nor can they be fetishised as an example of people's ability to build their own housing. These are conditions forced upon the poor, and often controlled by slum lords and land sharks who have regulated space allocation and permission for construction. The conditions of living are of bad quality, at least in the big metros, and basic civic services are not easily accessible. It is likely that the same situation will be seen in the smaller cities which are growing with the same lack of direction. The state must take the initiative to transform and rebuild slums through participatory planning thus radically improving the quality of life of workers and their families. The Telangana example is worth looking at, as is the experience of Kerala. Housing development is an effective democratic means for the achievement of this objective. Therefore, while urgent relief is important and must be provided, the larger political struggle for dignified housing and the achievement of land-equity rights cannot be given up.

Housing is meaningless unless it is accompanied by basic civic services, i.e. water, sanitation, electricity, roads, lighting, transport. Public services are integral to any housing scheme. Planning is therefore an important component of decent housing, to ensure open spaces, schools and hospitals at accessible distance.

Another factor is quality of housing. There is unstated understanding that underlies provision of housing or indeed any benefit for the poor- that it need not focus on quality.

A number of houses built under the earlier JNNURM are lying vacant, and people to whom they have been allocated have not moved in. There are many reasons for this: the houses are badly constructed, they do not have access to basic services, and they are far away on periphery of the city with no cheap transport and reliable transport to ferry people to their place of work.

Working Peoples aspirations and demands

The class system in India is not sustained by the state alone. It is based on caste hierarchies inherent in the Indian ethos. It took the relentless images of millions of people, many with naked feet, marching on the highways, heading home, hundreds of kilometers away for media and middle-class conscience to be shaken awake. Hopefully this will result not just in better emergency relief but in resolution with some of the deep seated long term issues, of better conditions of living for workers.

In the background of the COVID pandemic, policy makers cannot ignore the importance of decent housing any more. The health hazard posed by overcrowded living spaces, lack of proper sanitation, space and ventilation, in the slums and tenements of big cities speaks clearly of the need for urgent and radical measures. It is clear that if people are forced to live the way they are, if cities refuse to provide decent living to people who power the Indian economy, the health crisis it poses will also affect those who dwell in upscale neighbourhoods. This alone has the potential for becoming an impetus for changes in the living conditions of the working poor. This particular point makes me quite uncomfortable- why are we reinforcing existing stereotypes of disease coming from poor people? In fact we should condone this type of ‘impetus’ and insist that decent housing is a right for everyone.

The government is in the process of formulating a policy on the utilization of the package that has been been announced, ostensibly for the poor, especially migrant workers.

**The WPC puts forward the following demands and priorities in this regard:**

1. Upgrade existing settlements, while keeping in view the needs of livelihood, and provision of basic services
2. Provide easy access to basic free/easily affordable healthcare in every locality
3. Reserve land for low cost housing with provision of all basic amenities, including hospitals, educational faculties, green open public spaces
4. Create more rental housing. Set up bodies to build/ lease manage rental housing in all cities.
5. Allocate land specifically for public housing/housing for the poor in both urban and rural areas. Make provision for special needs.
6. Make builders liable to create more affordable housing especially for the poor.
7. State led PPPs to create more low-cost housing and rental housing. This implies caps on profits in the affordable housing sector.
8. Draft, regulate and strictly implement city planning norms. Focus on comprehensive city planning with a proper land use policy. Ensure right to space for all citizens not just the rich and upper middle class.
9. Turn vacant housing in both state and private sector into rental housing in space starved metros.

**You can download the statement through the link below:**

[WPC Statement on Rental Housing](https://drive.google.com/file/d/188ZX8_pIe0nsURndtQC4MzZO7BQVBqlQ/view?usp=sharing)