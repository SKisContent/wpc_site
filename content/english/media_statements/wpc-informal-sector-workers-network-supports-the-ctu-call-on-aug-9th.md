+++
bg_image = "/images/img-20200303-wa0074-height-500.jpg"
date = 2020-08-07T18:30:00Z
description = "##### The Working Peoples Charter Network, representing over 100 small and medium organisations working with informal sector and migrant workers in various states in India extends an ‘in- principle support’ to the call of the National Platform of Central Trade Unions to observe 9th August as “SAVE INDIA DAY”."
image = "/images/img-20200303-wa0074-height-500.jpg"
tags = []
title = "WPC - INFORMAL SECTOR WORKERS NETWORK SUPPORTS THE CTU CALL ON AUG 9TH! "
type = ""

+++
### **WPC - Informal sector workers network supports the CTU call on Aug 9th in principle**

###### The Working Peoples Charter Network, representing over 100 small and medium organisations working with informal sector and migrant workers in various states in India extends an ‘in-principle support’ to the call of the National Platform of Central Trade Unions to observe 9th August as “SAVE INDIA DAY”.

###### It also wholeheartedly supports the Scheme worker's unions/federations (Anganwadi, ASHA, Mid Day Meal etc) and calls for a two-day strike on 7th and 8th August 2020.

###### The WPC Network supports the CTU call to oppose the rampant privatization of public services. At the same time, WPC also calls upon all its members and also the CTUs to actively focus on the critical issues facing the informal sector workers, especially migrant workers in the current situation and to press for change to deal with the life and death issues facing this sector which comprises over 400 million today.

Accordingly, the WPC will support the demands set out for Aug 9th and also add three critical demands of the informal sector workers:

* Ensure that all workers working in the informal sector are covered by the ESIC to make sure that they have minimum health protection. This also means that ESIC must be strengthened, and the hospitals and care improved.
* Ensure work and a minimum wage of not below Rs 18000, for ALL workers
* Take housing for informal workers as a policy priority and work on centre-state solutions to make sure all workers and their families have habitable, safe, viable and affordable housing in all cities.
* WPC units will actively demonstrate their support in the different states.

#### In other states also similar actions will be organized. Updates will be available on [Twitter](https://twitter.com/IndiaWpc) and on the website:

Twitter @indiawpc Facebook: Indiawpc

### **Please download PDF file here:**

###### [WPC SUPPORT CTU 'SAVE INDIA DAY' 9TH OF AUGUST](https://drive.google.com/file/d/1Hb8p7KwaNAUbpe_QOZ6Dn5_r6vzNNA_r/view?usp=sharing)

Baba Adhav, Nitin Pawar, Madhukant Patharia, Chandan Kumar(Maharashtra), Meena Menon, NP Samy, Abhay Kumar, Nalini Shekar (Karnataka), Rajiv Khandelwal (Gujarat) Mewa Bharti, Santosh Poonia (Rajasthan) Ramendra Kumar, Arbind Singh Anita Juneja, Dharmendra Kumar, Nirmal Gorana (Delhi) P Chennaiya, Sister Lissy P, Varghese Theckanath,, Sashi Kumar (Telangana and AP), Sandeep Khare, Yogiraj Patel, Arvind Murthy (Uttar Pradesh), LA Samy, MA Britto (Tamil Nadu) Barnabas Kindo (Assam), and other network worker organisations.

**Picture Credit: National Centre for Labour (NCL)**