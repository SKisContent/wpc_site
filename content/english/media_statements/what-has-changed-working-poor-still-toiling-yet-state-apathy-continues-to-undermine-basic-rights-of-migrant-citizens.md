+++
bg_image = "/images/w.jpg"
date = 2022-03-24T06:30:00Z
description = "A statement on the condition of India’s migrant workforce two years after the COVID-19 lockdowns"
image = "/images/139160-vekcyivyru-1585498637.jpeg"
tags = []
title = "What has changed? Working Poor still toiling, yet state apathy continues to undermine basic rights of migrant / CITIZENS!!"
type = ""

+++
### What has changed? Working Poor still toiling, yet state apathy continues to undermine basic rights of migrant / CITIZENS!!

A statement on the condition of India’s migrant workforce two years after the COVID-19 lockdowns

**If the deaf is to hear, the sound must be loud & clear**...... Remembering these powerful words of Shaheed Bhagat Singh on the 91st death anniversary, who along with Sukhdev and Rajguru, stood firm for the cause of our country. Today, **the Working Peoples’ Coalition (WPC) agonizingly marks the second anniversary as a ‘Remembrance Day’ of the government's failure in addressing the lockdown-caused crisis of the migrant population**. Beyond promises for state elections and big-ticket pronouncements, we insist that this dark anniversary be used to ask difficult questions that seem to once again be leaving our minds as the pandemic recedes.

The WPC is not limited to any symbolism of just remembering the millions of migrant workers and their families who walked barefoot hundreds of miles on the national highways but also is committed to striving for a meaningful resolution to the issues which gave rise to those precarious conditions in the first place.

The WPC firmly stands up to the injustice and enforced violence on the working people despite repeated attempts to systematically veil & bury the toil of billions in the national growth. The WPC would continue to keep reminding those in power & authority – whether in the government, in the market or the society – that the apathy towards working people cannot be allowed to endure. The government, especially one that propagates repeatedly in various electoral platforms of caring and protecting the working people, is obligated to provide emphatic answers, especially to the migrant workers who are the backbone of the national economy and responsible for the country’s economic growth.

The simple truth is that we have failed the workers on whose behalf we felt empathy and outrage when they became visible on our highways, refuting the images of India’s growth story**. It is the abrogation of ‘Directive Principles of State Policy, the denial of fundamental rights, human dignity, and socio-economic justice to working people of India which is truly ‘anti-national’**. where the state provides for all citizens and takes special care of those who are at the margins and belong to historically disadvantaged communities. Leave aside provisioning welfare, the government has completely robbed the working classes of whatever little dignity they had- through the lockdown and the two years after (Annexure).

The WPC condemns the continuing apathy of working people and calls upon the union and the federal governments for immediate responses to the deteriorating condition of the working people across India.

* After the lockdown**, informal workers in India suffered a 22.6% fall in wages**, even as formal sector employees had their salaries cut by 3.6% on an average, according to a report by the International Labour Organisation (ILO). Even before the pandemic, the growth of the informal sector was sluggish due to demonetisation, however, the pandemic spelt disaster for the informal sector.
* No state has progressed beyond publishing Draft Rules under the four labour codes passed in 2019 and 2020, and the Central government is yet to finalise its Draft Rules and notify the Codes too. At a crucial time for workers, **labour governance architecture is in a state of paralysis right now with stakeholders not clear as to what laws to institute and abide by.**
* **Many draft policy documents highlighted the work conditions and lack of justice** for migrant workers, including [NITI Aayog's draft policy](https://theleaflet.in/niti-aayogs-draft-policy-on-migrant-workers-sees-migration-as-part-of-development/#:\~:text=The%20draft%20policy%20prescribes%20two,own%20natural%20ability%20to%20thrive%E2%80%9D.) on migrant workers, but they **are still waiting to see the light of the day.**
* Affordable Rental Housing Complex (ARHC) was announced on 20th July 2020 as a relief measure to the mass exodus of migrants from urban centres. **Close to 2 years into the implementation of the scheme the performance is a dismal 6.55%.**
* Programmes such as the public distribution system (PDS), specific relief schemes, or crisis cash transfers, proved to be inadequate or excluded many informal workers either because they weren’t recognised as workers or because of their interstate movement. Most of the workers had received assistance from civil society and non-governmental organisations (NGOs), either directly or through workers’ unions. **A telling empiric is that even after receiving the government cash transfers, a large majority of households had to take further debt to meet the expenses.**

The Union government in collaboration with federal state governments must urgently address all the issues, demands and concerns mentioned above. WPC calls upon all the worker organisations, citizens’ forums, and other civil society organisations to stand with migrant citizens. The Working Peoples’ Coalition (WPC) stands as a collective voice of informal workers in India who are demanding equity, social justice, and dignity. The WPC would persevere with an unflinching commitment to the ideals of this Sovereign Socialist Secular Democratic Republic. The WPC is the united front of the historically marginalized – but - inevitable future of this country – ‘the working people.

**_Working Peoples’ Coalition (WPC) National Secretariat_**

Website: [https://workingpeoplescharter.in/](https://workingpeoplescharter.in/ "https://workingpeoplescharter.in/")

Email: [workerscharterprocess@gmail.com](mailto:workerscharterprocess@gmail.com)

Facebook: @IndiaWpc

Twitter: @IndiaWpc

Instagram: @IndiaWpc

Youtube Chanel: [Working Peoples Coalition](https://www.youtube.com/channel/UCCBvS21TCMAoI-qsLqVpSlw)

Contact no: 022-40129763

**Download the Full statement here:**

[What has changed? Working poor still toiling](https://drive.google.com/file/d/1mKbjz_6xTibsSItR6LOSX-EIZNqbNk7k/view)