+++
bg_image = "/images/w.jpg"
date = 2022-07-04T08:30:00Z
description = "July 4th, 2022, New Delhi: Delhi Chapter of the Working People’s Coalition (WPC) today released its two reports; one on the actual access to minimum wages by workers in Delhi, and another investigating the Mundka factory fire reporting on issues concerning the safety of the workers."
image = "/images/whatsapp-image-2022-07-03-at-10-06-14-pm.jpeg"
tags = ["labour_axis", "india_labour_line"]
title = "Public Release: Mundka Fire: Fact Finding Report & Access to Minimum Wages"

+++
#### Public Release

**Delhi Chapter of the Working People’s Coalition (WPC) today released its two reports; one on the actual access to minimum wages by workers in Delhi, and another investigating the Mundka factory fire reporting on issues concerning the safety of the workers.**

The report on access to minimum wages was based on a survey of 1076 workers in Delhi across four clusters, viz. domestic, construction, industrial and security guards in January and February 2022. It collected data and information on the awareness level and access to social security entitlements such as PF, gratuity, ESI, accident insurance etc.

This study was conceptualised by the Delhi Chapter of the Working Peoples’ Coalition (WPC) and was guided by the vast experiences of Delhi Shramik Sanghatan, Janpahal, Gram Vaani and Yuva. In 2015, the Delhi government, heeding the demands of the worker organisations, implemented the historical supreme court ruling, the Raptakos Brett judgement (1991) that upheld the implementation of ‘need-based minimum wages’ as prescribed by the 15th Indian Labour Conference (ILC).

However, the policy measure faced a lot of opposition from the powerful owners’ lobby, and the pandemic further weakened the implementation of the measure. The idea behind the study was to take a stock of the gap existing between claims and realties. The study found that 95 per cent of workers, despite having required skill sets, are not being paid a statutory minimum wage as stipulated by the Government of Delhi.

**More than two third of workers are not aware of these laws that strengthen their right to receive decent wages and 98 per cent of workers do not receive pay slips. 98% of female workers and 95% of male workers receive wages below the stipulated minimum wages.**

Further, the study found that more than 75 per cent of workers work in indecent working environments without sufficient facilities and insecure work site premises, which could lead to unhealthy industrial relations and welfare losses for workers. The average age group of respondents was 26-45 years of age, with the youngest being sixteen and the oldest being seventy-two. Nearly 13% of individuals in the sample were below 25 years, while 25% were above 45, which indicates that the majority of “youths” as demographic dividends of India are deriving their wage livelihood from the unorganised sector. More than 60 per cent of workers are below the primary level of education which would constrain their labour market mobility as well as deprive them of accessing skill development opportunities since many skill development programs require minimum secondary education qualification. **The study found that approximately 46% of workers earn monthly wages between Rs. 5000- 9000/- and around 74% of workers are able to save less than Rs. 500/- per month. This amount, the report suggests, is not enough to even get a social insurance scheme as provided by the Government of India, such as ESIC.**

The study also noted that only 2% of women workers earn wages more than the stipulated minimum wages indicating significant gender-based wage disparity in the unorganised sector. The study concluded that more than 90 per cent of workers are deprived of their social security benefits. It suggests that this can be attributed either to a lack of awareness among workers or lax enforcement of social security. Mr Ramendra Kumar of Delhi Shramik Sangathan and vice president of Working People’s Coalition lauded the Delhi government for fixing need-based minimum wages but expressed disappointment at the non-enforcement of the minimum wages for workers in Delhi. He urged the government to prioritise strengthening labour administration for proper implementation of the minimum wages including for unorganised and migrant workers.

Following the Mundka factory fire on 13 May 2022, where 27 workers, of which 21 were women, passed away, 40 workers were gravely injured and more reported missing, a group consisting of labour NGOs, trade unionists, women’s rights activists, public health experts and planners conducted a detailed investigation into the Mundka factory fire tragedy, its causes, its impacts and the long-term consequences on the workers. The fact-finding team of WPC found that the four-storey building was being used for industrial purposes in the extended Lal Dora area where this is not permitted, without a valid factory licence.

It was acting in violation of all labour laws and safety regulations, as it was in a congested area, without any sufficient fire safety mechanisms in place and without a No Objection Certificate from the fire department. The fibreglass facade of the building was unbreakable during the fire and there were no windows in the building. The team found, via conversations with victims’ families and survivors, that the factory paid its workers, 90 per cent of whom were women, below minimum wage and without pay slips. This was just one, among a host of labour code violations by the factory. Most employees did not have any documentation that they were employed by the factory. The team noted that official figures of death were underestimated. Workers reported to the fact-finding team that more than 50 people were killed, of which 45 were women.

Ms Shruti of Jagori expressing shock over the incident raised the tragic tale of women workers and demanded exemplary punishment for gender-based discrimination at work. She advocated for constituting labour monitoring committees at central, state and local levels with due representation of women and informal workers. The team also interviewed survivors who were burnt and spoke of the mental trauma they endured being trapped in the fire and losing close friends. The team reported that all costs for treatment were being borne by the employees themselves with help from local NGOs.

Ms Kajal, a survivor of the fire, said that she and other survivors are yet to get any support from the government, and not a single penny has been given as compensation, forget about psychological and other rehabilitation support. The team concluded that this incident was entirely avoidable by proper enforcement of laws. In his address at the public release of the reports, Mr Sultan Ahmed of the International Labour Organisation (ILO) shared that the ILO has recently added a ‘safe and healthy working environment’ as a fundamental principle and rights at work. He said that all member states of ILO should commit to the fundamental right to a safe and healthy working environment.

Mr Dharmendra Kumar, National Secretary of, the Working People's Coalition, said that WPC will reach out to workers and register their grievances and will take appropriate efforts to ensure justice to workers. Mr Kumar further said that there can’t be any compromise on compliance with statutory provisions protecting workers' rights. He blamed the nexus between government departments and companies for the Mundka factory fire and demanded that all kinds of collusion between enforcement agencies and employers must be brought to an end and necessary fair affirmative actions need to be taken to regularise work sites.

##### **For more information contact:**

Dharmendra Kumar, National Secretary, WPC

\+91-9871179084

[Click here to download the Media Statement](https://drive.google.com/file/d/1o8j-pz1DsPp_5FTz95wFnsrhApos0kSe/view?usp=sharing)

[Click here to download the Mundka Fire Report](https://workingpeoplescharter.in/publications/delhi-mundka-factory-fire-a-culpable-homicide-occurred-due-to-states-negligence-and-ignorance/)

[Click here to download the report on Access to Minimum Wages](https://workingpeoplescharter.in/publications/accessing-minimum-wages-evidence-from-delhi/)

#### Media Coverage:

1. [Telegraph India](https://www.telegraphindia.com/india/over-95-per-cent-of-unskilled-workers-underpaid-in-delhi-survey/cid/1873196?fbclid=IwAR0zRm9C2bbZ1WaJSQcJCGj9wW9a4XtdvWjbuRLyOuJ-eXhZwlnk7_mS8xk)
2. [Workers unity](https://www.workersunity.com/news/mundka-fire-deaths-wpc-fact-finding-report-hint-rampant-forced-labour-in-delhi/ )
3. [News Click	](https://www.newsclick.in/Insecure-Work-Sites-Denial-Minimum-Wages-Survey-Highlights-Willful-Violations-Labour-Laws-Delhi?fbclid=IwAR3K4o8WDTDqbzFPgP1rdCOnIE3C-_jdWzZHxQK1XYHLcAgfc-R9wttvNtw)
4. [Workers unity	](https://www.workersunity.com/news/wpc-survey-only-5pc-labourers-get-minimum-wage-in-delhi/?fbclid=IwAR0LvHBDrqR6XLZhjegbtuL7w0ao_i2N-x9lI118_STx9tKEE4e_nBabGag)
5. [The Wire](https://m.thewire.in/article/labour/mundka-factory-fire-caused-by-negligence-many-havent-received-compensation-fact-finding-report?fbclid=IwAR0qECSooYDwlKeG2R7VSlwa3zB1gci07k_HvmzciDK7OWyRq4EvFe7Lre8)
6. [Jagran](https://www.jagran.com/delhi/new-delhi-city-ncr-mundka-fire-incident-the-organization-shared-the-fact-finding-report-fiercely-violated-the-rules-and-regulations-22864180.html? fbclid=IwAR32z3aCKoHtrVM6mdZt15HWgnM10JGI-bYzwqFrw6NQokMc_UX5clelsiM)
7. [Times of India	](https://timesofindia.indiatimes.com/city/delhi/95-of-workers-not-paid-minimum-wages-in-delhi/articleshow/92789450.cms)