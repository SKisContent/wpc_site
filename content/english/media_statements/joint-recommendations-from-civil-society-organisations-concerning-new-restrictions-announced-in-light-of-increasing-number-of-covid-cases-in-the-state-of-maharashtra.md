+++
bg_image = "/images/img-20200529-wa0058-height-500.jpg"
date = 2021-04-05T18:30:00Z
description = "We, civil society organisations and activists from Maharashtra, who are engaged in human rights work with different communities and have also been extensively involved in relief work and advocacy throughout the pandemic and consequent lockdown, address this letter to you to put forth some of our pressing concerns regarding the new restrictions announced in light of increasing number of COVID cases in the state of Maharashtra. We also request for a personal meeting with you to discuss our below suggestions and explore possibilities of civil society collaborations through constructive engagement and consultation, with an aim to create an effective response to the pandemic and resulting crisis, in the interests of the people."
image = "/images/img-20200529-wa0058-height-500.jpg"
tags = []
title = "JOINT RECOMMENDATIONS FROM CIVIL SOCIETY ORGANISATIONS CONCERNING NEW RESTRICTIONS ANNOUNCED IN LIGHT OF INCREASING NUMBER OF COVID CASES IN THE STATE OF MAHARASHTRA"
type = ""

+++
6th April 2021

To,

Shri. Uddhav Thackrey ji,

Hon’ble Chief Minister,

Government of Maharashtra,

Mantralaya, Mumbai 400032

##### **Subject:** **Joint recommendations from civil society organisations concerning new restrictions announced in light of increasing number of COVID cases in the state of Maharashtra**

Respected Sir,

We, civil society organisations and activists from Maharashtra, who are engaged in human rights work with different communities and have also been extensively involved in relief work and advocacy throughout the pandemic and consequent lockdown, address this letter to you to put forth some of our pressing concerns regarding the new restrictions announced in light of increasing number of COVID cases in the state of Maharashtra. We also request for a personal meeting with you to discuss our below suggestions and explore possibilities of civil society collaborations through constructive engagement and consultation, with an aim to create an effective response to the pandemic and resulting crisis, in the interests of the people.

We welcome the decision of the state government not to impose a complete lockdown as per the new guidelines issued on 4th April 2021. Your efforts at avoiding any hasty decisions to impose a lockdown and inviting solutions from civil society to deal with the pandemic crisis is highly welcome and appreciated. However, there is not much clarity on the state of crisis faced by migrant workers, daily wage earners and many other workers in informal sector due to the new restrictions.

From the experience and learnings of the national lockdown imposed by the Central Government while working with communities, we have witnessed firsthand the brutality of a complete lockdown on the majority poor population of the State. Especially vulnerable are the millions of migrant workers and daily wage workers, the homeless and marginalized groups who are badly affected as they no do not have the capacity to survive it without adequate arrangement for food, shelter and other basic amenities and necessary items. A situation of lockdown would once again result in loss of livelihood, income and lack of access to basic survival needs, which would severely affect the majority poor, who are yet to recover from the impacts of the national lockdown. It is not something our state can afford and the people of Maharashtra can endure once more. In our view, such a situation should be entirely avoided and instead, alternatives should be explored and measures put in place to ensure that the public health emergency and economic crisis on account of the pandemic do not worsen.

Even the increased number of restrictions will have and are already having an adverse impact on the most marginalized people in the state. It is of paramount importance that essential services and livelihoods are not disrupted this time around. In our view, the restrictions should be put minimally and only as required, in areas that are most affected, and not in a blanket manner. Restrictions should be imposed in a phased manner as per trends and clinico-epidemiological profile of COVID -19 in the state. Even where such restrictions are imposed, there is a need for transparency on the manner and criteria on the basis of which the restrictions are imposed. It is important to ensure that no matter the nature or extent of the restrictions, basic amenities, services and supplies reach the people even in the worst affected areas or containment zones.

It would be more effective to adopt a decentralized approach to deal with the pandemic, in consultation with the communities through associations, societies and groups of residents in the area. Greater community involvement and civil society engagement in preventive and promotive efforts in relation to the pandemic is bound to show better results. Community representatives and civil society organisations should be included in the task force and provided movement passes. We need area specific and area suitable lockdown norms, while keeping in in mind the food and shelter requirements in the economically weaker areas or neighborhood. No documentary proofs should be asked for giving the benefits of the relief packages in the poor neighborhoods under lockdown. Adequate arrangements for food, water and shelter should be put in place before imposing any lockdown conditions.

We are aware that the central government has not given the Maharashtra state government the amount from the provisional GST collection that is actually due to the Maharashtra government for the year 2020-21, which amounts to over Rs. 22,000 crore. We, as an alliance and networks consisting of over 100 people's organisations and civil society organisations, are willing to stand by you in demanding that the Central government pays up also a major part of the expenses that the state government has incurred in 2020 for COVID-19 related expenses -- medical and hospitalisation expenses, rations, transport etc. and are prepared to support this demand to be put out to the central government.

In addition to the above, we are submitting some specific suggestions with regards to the new restrictions and other burning issues in the context of pandemic, after due consultation and deliberation based on our ground experience and learnings from the national lockdown situation last year, with a view to both avoid a complete lockdown and ensure that the crisis faced by the people earlier is not repeated.

##### **To prevent a migrant workers crisis again**

We should not forget the plight of migrant workers, who were worst affected during the national lockdown imposed with barely any notice. Panic amongst the migrant workers is already being reported from different cities. It is hence, of utmost importance that the government instills confidence in them and takes steps towards making adequate arrangements. It is both the need of the city as well as informal workers and migrants that they continue to feel secure and at the same time earn their livelihoods. Therefore, we propose that before imposing any lockdown restrictions (partial or total) which will result in migrant workers losing their livelihood, income and food security and right to movement or their right to be able to return back to their homes, it is pertinent that the government should –

1. Provide adequate notice and put in place a proper system to ensure safe travel of migrant workers to their hometowns/villages. It is already being reported with the imposition of partial restrictions and uncertainty around the lockdown, that migrant workers have started flocking railway stations and exit points in cities to go back to their villages.
2. Provide food security to migrant workers through the public distribution system. The past experiences show that large number of workers with and without ration card were left to fend for themselves because of difficult rules of the government. Ration card portability issue needs to be resolved through orders/directions to rationing officers, to enable migrant workers to access rations in the city of their work. Rations should also be made available to non-ration card holders. There is a need to establish food centres/banks in every ward of the city with multiple counters after surveying the requirement ward wise.
3. Re-establish shelters for migrant workers who have lost or are in need of housing, with adequate facilities/amenities.
4. Provide for sustenance monies/cash transfer to migrant workers, including the majority workers who are in the informal sector, unorganized and lack registration. In order to do this, it is important to tie up with unions, associations and bodies of employers/workers to be able to identify and register workers, and ensure that support reaches them. The informal sector is the worst affected by the lockdown and has not yet recovered fully from the loss. Report suggests that not only the informal workers have lost their jobs but have left them debt ridden heavily. Due to poor documentation and lack of registration of informal workers in the system, informal workers are not able to enjoy the benefits of any social security schemes or relief packages in critical times like today.
5. Constant monitoring/survey of the impact of the lockdown restrictions on migrant workers and their livelihood status, to ensure support reaches them.

The government must come out with details of all such arrangements before imposing lockdown restrictions that will impact migrant workers in the state and should also ensure effective communication of governmental decisions/orders so that migrant workers are not left in a lurch.

##### **To ensure food security for all**

1. Food grains / rations must be made available to all, irrespective of documents. During the last lockdown, sex workers and trans persons were denied access to any government schemes due to lack of documents. Those without ration cards, those whose ration cards have been cancelled or with ration cards in other states, should all be able to access rations through the public distribution system.
2. No one should be deprived of ration just due to lack of Aadhar seeding or thumb impression not working on POS machine.
3. Saffron ration card holders should be provided with ration as per April 2020 rates keeping in mind the Covid 19 crisis .
4. Oil, fuel, cereals, other grains/dals, vegetables and other such essential items need to be included.
5. Area-wise food centres need to be set up to ensure cooked meals to the poor, to see to it that no one goes hungry.
6. Nutritious food / meals need to be provided. Poor people with special dietary needs for example lactating mothers, malnourished children, pregnant women etc. were completely ignored during the last lockdown.
7. Homeless persons were left without food and water for days during the national lockdown and in some cases when some of them were forcefully sent to under-construction buildings for shelter, either stale food or no food and water was provided. There is a need to ensure homeless persons are provided with food, water and basic amenities during the entire period of the lockdown restrictions.
8. Shivbhojan Yojana should be extended to tahsil and weekly market areas in rural Maharashtra.

##### **Housing, shelter and amenities**

1. Rent moratoriums need to be reapplied/ extended and should be applied in slums and chalis and to industrial units as well, to offer protection from eviction. Lakhs of such units are on rent as well, and the small producers running them are running out of cash and savings due to the continued rent burdens and the lack of orders/payments from vendors given the uncertainty about the lockdown. Without a rent moratorium, many units would be forced to close and millions of workers would lose their livelihoods as well as shelter, since over 70-80% of them live onsite.
2. Government-run shelters need to be provided for workers, homeless and poor who have lost their livelihoods and are in need of housing, with all basic amenities and adequate facilities.
3. Utility bills must be suspended for poor consumers
4. Access to water must be ensured for all, especially the poor populations. Access to water is crucial to deal with this public health emergency as well.

##### Health care

1. Health care access must be strengthened - for both COVID and non COVID care. There is a need for effective bed allocation and regulation of the private sector to ensure that people can access affordable healthcare.
2. While 80 percent of all beds under private sector were taken over by the state government and brought under price capping for COVID care, there is a need for better implementation and close monitoring of this policy and robust grievance mechanisms in every district - where complaints regarding overcharging can be made and such complaints must be disposed of in a time bound manner.
3. Non-Covid healthcare became almost non-accessible to the poor citizens during the national lockdown. While many facilities were diverted for COVID care, poor citizens were forced to access expensive private hospitals to avail of medical facilities. There is an urgent need for capping non-COVID care to ensure private hospitals do not commercially exploit this situation. There is also a need to set up medical centres in the urban slums and other populated or neglected areas to attend to healthcare needs of the communities.
4. There is also an urgent need for better implementation of the MJPJAY scheme to deal with the number of bottlenecks at ground level that deprive people from availing benefits under the scheme and for robust and accountable grievance redressal mechanisms.
5. The ambulance service should be strengthened and expanded, especially in districts with high case load and in rural/tribal areas. There has to be a strict monitoring mechanism of existing services.
6. The government must also urgently augment its own facilities and expand quality public health care services, especially in areas where they are not available.
7. Vulnerable communities such as the elderly, persons with disabilities, persons suffering from illnesses, must be provided access to quality healthcare. Those who cannot afford medical treatment must be ensured health care services free of cost.

##### **Vaccination**

1. Awareness and outreach would help in ensuring that people come out for vaccination. There needs to be increased awareness about the safety of the vaccine and a decentralized system for distribution. There is a very poor turn out from the poor neighbourhoods of Mumbai due to lot of misinformation or lack of information about vaccination. The healthposts, public announcements and other means through community engagement should be adopted to enable people to make an informed choice about taking the vaccine.
2. There is a need to set up vaccination centres in populated slums and localities, to increase access to vaccination.
3. The documentation burden to access vaccination must be removed for vulnerable groups (including homeless communities).
4. A help desk and helpline needs to be set up ward / area wise to ensure all queries regarding vaccination are answered. Technology can play a major role in bridging the information gap. Electronic messages, audio recordings in local languages can be used so people can access information easily.

##### **To minimize effect of current restrictions on workers and livelihood**

1. The previous guidelines which provided that the manufacturing sector can operate at full capacity and even do night shifts to help maintain social distancing norms, were not being implemented correctly. From the experience at Sakinaka’s larger informal manufacturing sector, the police forcibly shut units down by 6:30-7pm, with many incidents of violence. A situation like this, involving wrongful implementation, unnecessary harassment and use of force, could potentially lead to many units shutting down, resulting in economic hardship. There must be clarity in terms of guidelines and accountability from enforcement agencies to ensure the most vulnerable are not targeted.
2. There is a need to have special protection for hawkers, naka workers whose access to livelihood depends on access to space (to sell, congregate) in the city. Similarly those workers whose jobs have been rendered vulnerable by the time restrictions already imposed need to be identified and they need to be protected from the impact of livelihood loss and ensure food, housing and income security.
3. Cash transfers/sustenance monies need to be provided to those who are already facing economic insecurity and income loss.
4. The state government should give an emergency allowance to every person who has lost his/her livelihood or does not have a livelihood.
5. During last lockdown, the daily wage earners, construction workers were not paid their due wages, in spite of filing complaints. This time the labour department should be given directions to initiate a process to take proper cognizance of these complaints and to take proactive measures to help contract workers and migrant workers.

##### **Coordinated and humane implementation of the new restrictions**

1. Essential services within the containment zones must be ensured by the government in collaboration with the local civil society organisations, elected representatives, area sabha members and community leaders.
2. In case the implementor of these restrictions is the police, they should be oriented for more humane implementation of restriction rather than curfew style that they are used too.

##### **Setting up of 24-hour helpline**

A 24-hour helpline should be set up to deal with specific issues facing people due to the pandemic and resulting lockdown restrictions. It should be ensured that the helpline is functional and effective in troubleshooting and finding solutions for callers.

We trust that you will consider our suggestions, which are not exhaustive but an attempt to understand and find solutions collectively to the crisis facing us all through this second wave and the future course of the pandemic. We offer our cooperation and support in your endeavours to alleviate the crisis for the people of Maharashtra.

##### **Yours sincerely,**

* 
* Ajeevika Bureau
* Anna Adhikar Abhiyan, Maharashtra
* Auxilium Skills Academy
* Bebaak Collective
* Beghar Adhikar Abhiyan
* Bharat Jodo Abhiyan
* Bombay Sarvodaya Friendship Centre
* Centre for Promoting Democracy
* Centre for Study of Society and Secularism
* Dev Kripa Mandal, Mumbai
* Dharavi Recylers Collective
* Forum Aainst Oppression of Women
* Franciscane Sisters of Mary Social Service Society
* Ghar Bachao Ghar Banao Andolan
* Habitat and Livelihood Welfare Association
* Hamal Panchayat, Pune
* Helping Hands Charitable Trust
* Homeless Collective
* Jan Andolano Ki Sangharsh Samiti
* Jan Swasthya Abhiyaan (JSA), Mumbai
* Justice Coalition of Religious, Maharashtra
* Maharashtra Rajya Beghar Abhiyan
* Mumbai Responds Network
* National Alliance of People’s Movements, Mumbai
* National Centre for Advocacy Studies
* Pani Hakk Samiti
* People’s Union for Civil Liberties, Maharashtra
* Platform for Social Justice
* Sahayini Social Development Society
* Sahara Aalhad CFRCAR
* Sahara CFRCAR
* Sankalp Rehabilitation Trust, Mumbai
* Sarvahara Jan Andolan
* Shoshit Jan Andolan
* Shramik Mukti Dal (Lokshahivadi)
* Trade Union Solidarity Committee
* Working People’s Charter, Maharashtra
* Youth Feed India
* Youth for Unity and Voluntary Action
* WPC