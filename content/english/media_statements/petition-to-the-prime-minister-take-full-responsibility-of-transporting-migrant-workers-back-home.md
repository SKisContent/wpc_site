+++
bg_image = "/images/img-20200529-wa0060-height-500.jpg"
date = 2020-05-27T18:30:00Z
description = "##### The petition to the Hon’ble Prime Minister Shri Narendra Modi, endorsed by over 4,000 signatories including eminent scholars, retired civil servants, former members of the judiciary, public figures, labour activists, labour organisations and concerned citizens from across India has called upon the Government of India to fulfil its constitutional obligations and facilitate the safe and dignified movement of millions of migrants attempting to reach home by using central forces, facilities, and resources."
image = "/images/img-20200529-wa0060-height-500.jpg"
tags = []
title = "PETITION TO THE PRIME MINISTER TAKE FULL RESPONSIBILITY OF TRANSPORTING MIGRANT WORKERS BACK HOME"
type = ""

+++
**The petition to the Hon’ble Prime Minister Shri Narendra Modi, endorsed by over 4,000 signatories including eminent scholars, retired civil servants, former members of the judiciary, public figures, labour activists, labour organisations and concerned citizens from across India has called upon the Government of India to fulfil its constitutional obligations and facilitate the safe and dignified movement of millions of migrants attempting to reach home by using central forces, facilities, and resources.**

A copy of the petition sent to the Prime Minister is enclosed with this press note. Also enclosed is a list of some of the eminent signatories who have endorsed this petition. The petition was live online till about 3:30 pm today, the 27th of May 2020. Some of the eminent signatories to this petition, include.

Ever since the lockdown was announced, a humanitarian crisis unprecedented in India’s modern history, has severely disrupted the lives of India’s migrant workers. Millions of migrants have found themselves stranded without food, cash, and shelter, trying to get home. They have been subjected to violation of their fundamental rights under Articles 14, 15, 19, ans 21 and often to severe police harassment on interstate borders. Many have reportedly died as a result of the lockdown, due to exhaustion en route home, starvation, suicides, police excesses, illnesses, and rail and road accidents. As per reliable [sources](https://thejeshgn.com/projects/covid19-india/non-virus-deaths/), as many as 667 non-COVID deaths have occurred across the country. 205 of these have occurred among migrant workers en route on foot, and 114 due to starvation and financial distress. Meanwhile, the current response strategy with the provision of shramik trains, has been inadequate at best. [Over 2,000](https://timesofindia.indiatimes.com/india/after-2050-shramik-runs-just-30-of-migrants-have-managed-to-leave/articleshow/75878703.cms) shramik trains have reportedly carried over 300,000 migrant workers in the last week. This accounts for about 30% of the total population. Further, this does not include those who are en route and on foot, or those who are stranded in shelters or on interstate borders, or elsewhere. As of this moment, the government of India does not seem to have any estimates on the total number of people stranded and/or en route home across the country nationally as revealed in a [recent RTI](https://www.thehindu.com/news/national/no-data-of-stranded-migrant-labourers-with-labour-commissioners-office-labour-ministry-to-rti-activist/article31516386.ece).

This petition appeals to the Prime Minister and the Government of India to fulfil their constitutional responsibility for the issues of inter-state migration and inter-state quarantine, listed as Item 81 in the Central schedule. It urges the central government to take leadership in this critical hour, in co-ordination with states, to ensure a safe and dignified transportation of migrant workers, including the provision of assistance in their pre-departure and post-arrival arrangements. The safe transportation requirements of this magnitude are only possible if the forces and facilities at the disposal of the Government of India are immediately deployed to bolster the grossly inadequate efforts being currently left to inter-state coordination, with meagre logistical support in providing safety, food and water to migrants on the move.

There are several strong reasons that form the basis of the appeal:

First, the current predicament demands the same resources of the central government as would any natural disaster. In the wake of the COVID-19 spread, the National Disaster Management Act has been invoked to implement emergency measures such as the lockdown. There is sufficient evidence to suggest that the threat to life and livelihood, and the projected rates of starvation, illnesses, and wage loss, warrant responses befitting a nation-wide disaster. Defence experts and scholars have previously argued for the same. In 2018, Ganesh Kumar and Brigadier Ravi Dimri of the Indian Military Academy published an [important paper](https://ndpublisher.in/admin/issues/EAv63n3v.pdf), elucidating why the deployment of central forces is warranted in such situations, and how the timely deployment in the past has helped mitigate several challenges.

The National Disaster Relief Force along with other Central Forces should be included in the process of managing this humanitarian crisis, even if for a limited time, to support the state and central government machinery.

Second, in the current operations set up for the return of migrant workers, coordination between the centre and the states, and that between states has been rife with confusion and mismanagement. Notwithstanding the numerous orders, corrections, and revisions over guidelines on the Shramik special trains, the total number of trains have been insufficient. During the three weeks of Shramik trains operations only 30% of the migrants have returned home and in deplorable conditions during transit. The shortage of trains and a labyrinthine set of procedures have led to heightened desperation and subsequently rampant overcrowding at stations, rumour mongering over train schedules and the emergence of ticket sales in black across cities. Migrant workers are having to ask their families back home to send them money to pay for their travel. There are also reports of trains cancellations, diversions, prolonged delays in arrival. Trains have also been running without staff, amenities, food, and water over multiple days of commute. The situation has been compounded by the recent Amphan cyclone. According to estimates, despite the provisions made by governments and millions of migrants walking home, at least 20 percent are still stranded in different locations.

Various ground reports have claimed that around 4 to 6 million workers would need to return to Uttar Pradesh, and 1 to 3 million workers to Bihar alone. It is clear therefore that little has been done or can be done with the shramik trains arrangement so as to match supply to demand. There are just too many migrants and incidentally too few trains, aided with extreme lack of coordination and mismanagement.

Third, not all states have the same resources to manage the ongoing and projected scale of migrant returnees. In Bihar for instance, 119 trains brought 1,96,350 migrant workers on a single day. The government of Bihar has on a number of occasions said that it does not have the resources to bring everyone back to the state. The appeal urges the GOI to use all resources at its disposal and provides financial support to states for transportation and rehabilitation of migrants upon arrival. In this the facilities and resources of the central forces can provide a crucial buffer to the states.

The migrants need safe passage, food, water, and medical assistance in transit all of which is utterly failing them this crucial time. The proven logistical acumen of our central forces and the presence of its facilities and infrastructure across the country can offer the much-needed emergency transit support and medical help to migrants else they will continue to suffer acute hardship.

This appeal notes that the coordination and transit of internal migrants stands in stark contrast to the procedures adopted by the government to bring back migrants and Indian citizens stranded abroad. In their case, the procedures were extremely streamlined. Moreover, resources of our central forces, were efficiently used to transport stranded persons from foreign countries, and for medical help and quarantine facilities.

Finally, and importantly, there is a need to intervene beyond just provision of transportation. It is vital that the overall well-being of migrant workers is safeguarded as they attempt to reach home without jobs and savings. This includes emergency income transfers for a certain period and an urgent provision for employment and rehabilitation.

It is worth noting that Admiral Ramdas, former Naval Chief, Admiral Arun Prakash, former naval chief, and Mr. E. A. S. Sarma, former Union Finance Secretary are among those public figures who have in communications addressed to the Prime Minister or in media interactions, also recommended a similar course of action in this dire humanitarian emergency.

A compilation of recent media reports from across the country covering movement related delays, deaths, rioting, scams, and hardships to migrants is attached here in support of this appeal.

The Honourable Supreme Court has taken suo moto cognizance of migrants’ plight and has instructed the Centre and States to provide information on their provisions to support the migrants who are facing such extreme hardships. This is arguably a unique opportunity to act swiftly and to commission the services of central forces to save millions of mobile lives on the edge of complete disaster.

***

_For more information on this petition, please contact: Anhad Imaan (Aajeevika Bureau): +91 98118818284 Chandan Kumar (Working Peoples’ Charter): +91 9717891696_

_This petition has been carried out on the platform_ [_Jhatkaa.org_](https://jhatkaa.org/campaigns/)_. For more information, please contact: Nimisha: +91 7760609014_

***

**To download the relevant document please click on the below links:**

###### [**Press Note - English**](https://drive.google.com/file/d/1WUn9Kb3VHVNKukthYuMvhCVJUMiiqWOK/view?usp=sharing)

###### [**Press Note - Hindi**](https://drive.google.com/file/d/1nIjd2dsc6ktgm1BBZsj6rqv8X_QayG3V/view?usp=sharing)

###### [**Petition to PMO**](https://docs.google.com/document/d/1NTag6Vu6iyngCZFA7FDixHte7hntwgsN/edit?usp=sharing&ouid=101658157444468004586&rtpof=true&sd=true)

###### [**List of eminent signatories**](https://docs.google.com/document/d/1zwDN9JFc9OVys_-J0Wg7OcifcQ9irsFx/edit?usp=sharing&ouid=101658157444468004586&rtpof=true&sd=true)

###### [**Compilation of Media Stories**](https://drive.google.com/file/d/1rhjZ87J7A8MP-lALN6Js8hRXlkbihVoS/view?usp=sharing)