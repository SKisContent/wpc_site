---
title: Contact Us
bg_image: "/images/w.jpg"
description: 'RTI Description Text'
menu:
  main:
    name: RTI
    URL: rti
    weight: 16
    
---

**![](/images/whatsapp-image-2021-09-27-at-12-29-59.jpeg)**

#### **Right to Information:**

##### **A Democratic Gateway to Government Activities**

***

Working Peoples' Coalition (WPC) along with YouRTI is availing one-click access to file RTI requests online for free of cost on the website to ensure that anyone can seek public information from anywhere at any time.

***

For more details, contact: Chinmayi Naik. Email: [chinmayi.naik@indialabourline.org](chinmayi.naik@indialabourline.org)