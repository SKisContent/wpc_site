+++
bg_image = "/images/w.jpg"
date = 2022-07-04T08:30:05Z
description = "This report provides an overview of the income of unorganised workers from 4 sectors in Delhi - Domestic, Industrial, Construction, and Security. It examines wage gaps between different categories of workers, including gender wage gaps and gaps between labour market wage rates and the minimum wages set by Delhi state. Then it discusses the wage policies, legislations, and their implementation, focusing on minimum wages and collective bargaining. Based on this at the end, the report will provide some policy-oriented conclusions. "
image = "/images/0001.jpg"
source_url = "https://drive.google.com/file/d/1Y-BhXf_h2rlZQXkrQQvqB2oZhQf8viJl/view?usp=sharing"
tags = ["india_labour_line", "labour_axis"]
title = "Accessing Minimum Wages: Evidence From Delhi"
[[publishers]]
designation = "State Chapter"
image = ""
name = "WPC Delhi Chapter"

+++
#### **Highlights:**

* Of 1076 workers respondents, 57% (men) 43% (women) and 1 % (another gender) showing more than 50% per cent of men workers are working in across all industries; however, women workers are concentrated mainly in domestic work and construction sector.
* Two third of the sample youths in terms of demographic dividends of India are deriving their wage livelihood from the unorganised sector.
* More than 60 % of workers are below the primary level of education which would constrain their labour market mobility as well as deprive them of accessing skill development opportunities.
* Around 64% have migrated from their hometown in search of livelihood in Delhi. Of that 8 % of workers might have been part of circular migration due to the seasonality of industries.
* Approximately, 40 % work in domestic work, 16% in industry, 33% in construction and 11% as security workers in Delhi.
* Disproportionate educational qualification to secure good jobs and excessive supply of unskilled labour force compels them to stay in low-wage-low-productive sectors with bare minimum earnings.
* Only 5% of workers are receiving stipulated minimum wages and 95% are compelled to accept the wages offered by their employers, not by the regulatory bodies.
* It is found that 95 per cent of workers despite having required skill sets are not being paid a statutory minimum wage as stipulated by the Government of Delhi.
* More than two third of workers are also not aware of these laws that strengthen their right to receive decent wages and 98 per cent of workers do not receive pay slips.
* The study finds that 98% of female workers and 95% of male workers receive wages below the stipulated minimum wages.
* More than 90 per cent of workers despite working tirelessly are deprived of their social security benefits.
* More than two third of workers (more than 75%) work in indecent working environments without sufficient facilities and insecure work site premises, which could lead to unhealthy industrial relations and welfare losses for workers.

**Download the full report here:**

[Accessing Minimum Wages: Evidence From Delhi](https://drive.google.com/file/d/1Y-BhXf_h2rlZQXkrQQvqB2oZhQf8viJl/view?usp=sharing)