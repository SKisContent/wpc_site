---
title: Labour Axis
description: WPC works with a knowledge network called Labour Axis, which has researchers
  working on labour who work with the network to help in formulating positions and
  informing campaigns.
bg_image: "/images/w.jpg"
related_tag: labour_axis
type: ''
date: ''
image: ''
authors: []
categories: []
tags: []
facts:
  enable: false
  fact_item: []
menu:
  main:
    URL: labour_axis
    weight: 7

---
#### WPC has groups of experts to help with research, training, education and policy, to generate briefing papers and statements. There are different policy groups for different thematic areas. 

WPC works with a knowledge network called Labour Axis, which has researchers working on labour who work with the network to help in formulating positions and informing campaigns. We are setting up a digital platform for providing information w.r.t WPC and members’ activities, encouraging interaction between member organizations, which is also engaged in educating and training local leadership of the organizations working with informal sector workers. The WPC’s work on labour also includes an understanding and engagement with citizenship, sustainable development and the city. WPC works in a social and political context and the place of labour within it. It sees labour as an exploitative capitalist system based on profit above all else. It sees class in intersectionality with gender, caste, and race. It is committed to building a democratic, sustainable and climate-just world.

The COVID-19 scenario has reinstated labour issues in social, political and economic discourse in different sectors, such as affordable housing, employment guarantee, healthcare access etc. Therefore, an urgent and concerted set of steps is required to restructure the dynamics between labourers, industries and economic growth, to shift to a sustainable development path. The successful completion of this transformation will require thinking beyond today’s business-as-usual scenario. Moreover, transformative action is needed simultaneously at the global, regional, and national levels and in every research sector, including health, education, politics, gender, economic development, equity, and social inclusion. Although the extraordinary scientific and technological knowledge acquired in the course of labour movements for over a century can assist in tackling these challenges. Still, a tripartite engagement between state, industries and labourers/unions is necessary to ensure access of labour to their basic entitlements. This engagement is necessary because informal labour has not yet had access to basic entitlements. Momentum for transformation is required in the form of new dialogues between the labour academia, unions, governments, businesses and civil societies. Also, in the form of policies that are informed, powerful, implementable, and communicable - policies that would be based on focused and collaborative scientific research with clear and productive aims and outcomes.

##### On this background, Labour Axis aims to gather knowledge that is important for people working in the field of labour and celebrate the diversity of the country through the advocacy of migrant labour entitlements. 

Also, conduct action research on issues of importance for Working Peoples’ Charter (WPC) member-constituents. The main focus will be on organising collaborative research, commissioning independent research and publishing material in the area of labour. This will lead to many activities such as, conferences, seminars, webinars and round tables and publishing content for further discussion and debate on various subjects, including those which are indirectly related to labour. Apart from this, an important objective is to facilitate a neutral platform for discussions/negotiations between state/government, Unions/labourers and industries through a tripartite dialogue.

To aggregate the knowledge on labour and to make it widely available, this organisation will encourage artistic, creative expression around labour issues. This knowledge material will be available in different local languages to reach different parts of the country. It will also focus on migration as it is an important component of labour rights and organisation; and emphasize the intersectionality of labour with gender, race, caste, community, language, culture, etc.

##### Activities

* Generate, aggregate, publish, build collaborations in, knowledge and research Organise tripartite consultations/discussions
* Conduct training, study circles, workshops, exhibitions,
* Public policy briefs
* Produce videos and documentaries
* Organise social media outreach
* Organise events like:
* Public webinars
* Inter-sectoral (in labour), inter-union exchanges
* Discussions between labour and other social movements and sectors- e.g., women, caste, sexual minorities, etc
* Tripartite discussions between people from labour, industry, government and govt/parastatal agencies,
* Discussions, workshops on culture, social issues, politics, economics, etc Sharing of knowledge on best practices
* Cultural programs, film shows,
* Set up a website to aggregate the work and activities

##### Objectives

* To provide theoretical, analytical, and informative matters for the organisation and well-being of labour, especially informal labour.
* Facilitate a neutral platform for tripartite discussions between state/government, unions and industries Support the labour movement and advance solidarity at the national and international level Build international and regional (Asia, South Asia) solidarity of labour and social movements for change Bring about greater unity of action and analysis in the labour movement
* Develop dialogue between labour in different sectors and unions, between the formal and informal sectors Develop dialogue and understanding between labour and different social sectors Increase the capacity of labour to intervene in policymaking and social change Work towards restoring the voice of labour in social and political change in the country Supporting WPC constituents with sectoral research

##### Registration

Labour Axis is registered as a Public Charitable Trust, with the exemption under 12 A and 80 G (IT Act). The Trust can receive funds from Indian entities and individuals. Labour Axis is affiliated with the Working People’s Charter Network (WPC) and its work and priorities are decided in consonance with those of the network.

Labour Axis is focused on knowledge creation in the field of labour, on research, writing and training that has a practical and direct benefit to the labour movement in general and WPC constituencies in particular. It has a group of young researchers who are working on various projects and we are receiving small grants to coordinate and facilitate this and also pay researchers a small honorarium in some cases. There is a panel of senior researchers from various labour related areas who help with conceptualisation, methodology and outreach.

Before the end of the year, after the National Conference of the WPC in October, the Trust will focus on building its structure, work plan and goals for the year 2022. Applications for research grants on the key areas of interest will also be completed and submitted.

Since the inception of Labour Axis in April 2021, we have undertaken small yet sustained steps towards building a team of people from various like-minded organizations (such as YUVA, HALWA, Aajeevika Bureau), academicians and professionals working together on the various issues of the workers.