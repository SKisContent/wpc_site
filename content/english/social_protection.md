---
title: Social Protection
description: National Social Protection Floor
bg_image: "/images/w.jpg"
type: theme
facts:
  enable: false
  fact_item:
  - name: Workers Supported
    count: 60
  - name: Meals Provided
    count: 50
  - name: Funds Raised
    count: 1000
  - name: Workers Vaccinated
    count: 1400
success_story:
  enable: false
  bg_image: ''
  title: ''
  content: ''
  video_link: ''
menu:
  main:
    URL: social_protection
    weight: 8
    parent: home

---
##### The Government of India legislated the Code on Social Security 2020 ‘with the goal to extend social security to all employees and workers either in the organised or unorganised or any other sectors’. However, in reality, the Code continues to statutorily discriminate between organised and unorganised. We insist that this Code be made to guarantee universality and equity of social protection to all workers. Such a guarantee is well supported by, for example, ILO Convention 102 which may be supplemented by a National Social Protection Floor constituted in line with the ILO’s Recommendation 202 as ‘nationally defined sets of basic social security guarantees which secure protection aimed at preventing or alleviating poverty, vulnerability and social exclusion'.

1. We demand that the implementation of the Social Security Code 2020 provide for a clear and accountable institutional arrangement guaranteeing a gender-equitable, universal Social Protection Floor consisting of health and medical care coverage; pension; provident fund and gratuity; sickness, injury, disability and death compensations; maternity benefit; unemployment allowance; social housing benefit; and food security. The government must additionally strengthen and universalize the Integrated Child Development Services to promote equal opportunity for women to participate in the labour market.
2. We propose that this demand be met through harmonious integration of existing social protection and social assistance programmes like the Employees’ State Insurance Scheme, Employees’ Provident Fund, sectoral welfare boards and other central and state level programmes, as is the expounded goal of the Social Security Code.
3. The recent pandemic illustrated the need for protection of income for all workers in the urban centres. WPC advocates guarantee of income and employment for urban workers on the lines of Kerala’s Ayyankali Urban Employment Scheme that has been running for almost a decade. Himachal Pradesh, Orissa and Jharkhand have also introduced state-level employment guarantee schemes, which while being important steps forward, still remain limited in scope and intent. A comprehensive social safety net on these lines is a clear imperative.
4. In the interest of transparency, accountability and time-bound implementation, we demand that the concerned governments set up robust tripartite facilitation and monitoring mechanisms with due representation of unorganized workers.