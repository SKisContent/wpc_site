+++
bg_image = "/images/img-20200529-wa0058-height-500.jpg"
date = 2021-10-30T18:30:00Z
description = ""
image = "/images/img-20200529-wa0058-height-500.jpg"
location = "Delhi "
members = []
state_website_url = ""
title = "Maharashtara "

+++

* A physical meeting was organised in February (6th Feb 2021) in presence of all the people who initiated the process as well as have been a part of WPC since its inception. It was a day-long discussion of - Labour Codes, Workers Housing, Legal Issues, and discussions on structures and campaigns to be taken up in Maharashtra with 21 participants.


* 3 key areas emerged from a day-long discussion


* Wages and social security


* Housing for workers


* Expanding WPC to other areas in Maha. For structure in each state, we were asked to wait for the WPC Executive Committee decision.

  • A meeting was organised with Mr Sachin Ahir to introduce him to the work that WPC is doing in Maha and introduce him to various partners. We introduced him to the demands from various sectors.

• The purpose of the meeting was to make inroads into the government for advocacy for unorganised sector workers’ rights.

• Meeting with Mr Chaggan Bhujbal, Minister of Food, Civil Supplies, and Consumer Protection on 3rd June 2021 with a proposal to start community kitchens at 18 locations in Mumbai. Following up with the SC order of 24th May 2021 in the Suo Moto Case to provide for stranded migrant workers.

• Meeting with Mr Chaggan Bhujbal gave us an idea to run a workers’ canteen on the lines of Kashatich Bhakri and our experiences of the community kitchen. The need for nutritious affordable meals in COVID times for immunity to fight the disease and its direct relation to the realization of the optimum potential of the worker cannot be negated. A meeting with Honourable Labour Minister Mr Hasan Mushrif to run the canteen at Maha Labour Welfare Department was taken up. We are still working on the proposal.

• Meeting with Mrs Ashiwini Joshi, Labour Secretary Unorganised sector. The purpose of the meeting was to introduce WPC to her and propose to her a collaboration on the registration process of the unorganised sector workers. She informed us that she does not have the funds for the same and she has proposed to the State Government to provide her 5 crores towards the registration. She showed her willingness to collaborate.

• Following this, a zoom meeting was organised to introduce the diversity of people associated with WPC and to open a communication channel with her. The various stakeholders - construction workers, rag pickers, home-based workers, Gig workers, domestic workers, brick kiln workers, industrial workers, and others. Each stakeholder group presented its demands and gave her an idea of how many workers they can bring to the unorganised sector portal.

• Relief - 1-month community kitchen was run in May serving 4000 meals a day at Pimpri Chinchwad. 2 months community Kitchen at Mumbai in May and June

• 3 Webinars have been conducted since the ad hoc state committee has been functioning in Maharashtra.

• 18th March - Webinar - Covid and Housing

• Webinars in the series of Examining Workers Housing: Past, Present and Best Practices - Western India on 25th June the focus was on how low-cost housing in the state of Maha and Gujarat were created in the past.

• The second in the series was held on 13th August 2021 and focused on workers housing - Beedi Workers Housing, Sanitation Workers Housing and Mill workers Housing, Slum up-gradation projects in Gujarat, and SEWA - Mahila Land Trust Project in Ahmedabad.