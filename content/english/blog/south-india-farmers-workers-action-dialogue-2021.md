+++
authors = ["english/team/pratik-kumar.md"]
bg_image = "/images/untitled-design-1.png"
categories = []
date = 2021-12-17T18:30:00Z
description = "The Working Peoples’ Coalition (WPC) & the Tamil Nadu Land Rights Federation (TNLRF) together organised a South India Action-Dialogue of farmers, peasants and other informal sector workers focusing on “the Labour & Agrarian Sector Reforms and Strengthening Farmers-Workers Solidarity” on 17-18, December, 2021 at Asha Nivas Social Service Centre, Chennai."
image = "/images/1.png"
tags = []
title = "SOUTH INDIA FARMERS-WORKERS ACTION-DIALOGUE - 2021"

+++
**SOUTH INDIA FARMERS-WORKERS ACTION-DIALOGUE - 2021**

The Working Peoples’ Coalition (WPC) & the Tamil Nadu Land Rights Federation (TNLRF) together organised a South India Action-Dialogue of farmers, peasants and other informal sector workers focusing on “the Labour & Agrarian Sector Reforms and Strengthening Farmers-Workers Solidarity” on 17-18, December, 2021 at Asha Nivas Social Service Centre, Chennai.

The idea was to discuss the newly proposed labour and agrarian sector reforms, particularly in the context of fostering a meaningful solidarity between farmers and workers across the region in the informal sector which would eventually facilitate the goal of achieving “an universal social protection for all people”.

Around 150 participants/delegates from all geo-cultural and socio-political backgrounds from across the country converged, deliberated and shared in the 2 days long action-dialogue meet. The key speakers included: Prof. Yogendra Yadav, from Jai Kisan Andolan, Mr. Devinder Sharma, Agriculture Economist and Journalist, Ms. Harinder Bindu, from Bhartiya Kisan Union (Ugrahan), Mr Avtar Singh Mehma, from Krantikari Kisan Union, Navsharan Singh, Activist and Researcher, Prof. Jawahirullah, MLA, President, ManithaNeya Makkal Katchi, Prof. Babu Mathew, NLSIU, Bangalore, Prof Dr. K.R. Shyam Sundar, from XLRI Jamshedpur, Ms Ramapriya Gopalakrishnan Noted labour lawyer Madras High Court, Dr. R.V. Kumaravelu, from Fisherfolk and Coastal Rights Movement, Mr. C.Nicholas, from Panchami - Dalit Land Movement, Mr. C.R.Bijoy, from Forest and Tribal Rights Movement, Mr. M. Krishnamoorthy, from Salt-pan Workers Movement, Mr. Arul Arumugam, from Eight-lane Roads Movement, Mr. J.M Veerasangaiah, from Karnataka Rajya Rayatha Sangha, Mr. P. Murthy, Ex MLA from Sanitation Workers Movement, Pudhucherry and others.

The meeting concluded with the release of _a CHENNAI RESOLUTION_. The same is appended below.

WPC is intending to organize series of such consultations in the upcoming few months and are looking forward to your esteemed critical contributions for strengthening people’s solidarity particularly farmers-workers in India.

In Solidarity,

Meena Menon, Rajendra Bhise, Abhay Kumar, Bro Varghese Theckanath, Chandan Kumar and others

On behalf of Working Peoples Coalition (WPC)

L.A. Samy, M.A Britto and C.Nicholas

On behalf of WPC-Tamil Nadu & Pondicherry and Tamil Nadu Land Rights Federation (TNLRF)

##### **CHENNAI RESOLUTION**

**SOUTH INDIA FARMERS-WORKERS ACTION-DIALOGUE - 2021**

**The Working Peoples Charter Network & The Tamil Nadu Land Rights Federation**

**17-18 December 2021 (Chennai)**

Today’s India is retrogressing with the denial of labour rights, deterioration of democracy, violation of human rights, denial of social justice, rejection of pluralism, obliteration of secularism, fostering of hatred politics and nurturing fundamentalism. The rights that are already devolved to workers, fishermen, small entrepreneurs, _Dalits, Adivasis_, religious minorities, student communities, women, civil society organizations and human right activists, have been snatched away from them and the power is getting disproportionately centralized with the union government moving towards authoritarianism. The government is negating all those labour rights that have been acquired after protracted struggles nationally and internationally, amalgamating them with amendments in favour of private corporate giants. It is now imperative for all working people to oppose such disastrous anti-workers and anti-farmers move of the government. At this juncture, we are also impelled to protect the Indian Constitution which upholds the values of democracy, equality, social justice, socialism, human rights and the like.

We, the 130 delegates from Tamil Nadu, Kerala, Karnataka, Telengana, Andhra Pradesh and Pudhucherry, present at the SOUTH INDIA FARMERS-WORKERS ACTION-DIALOGUE have come together on 17-18 December 2021 in Chennai to raise the collective voice of such informal workers and farmers. We remind ourselves that the registration of informal workers is the first step towards, not only achieving social protection, but also recognizing long overdue labour rights as envisioned in our Constitution (Articles 37, 38, 39-A, 41, 42, 43, 45 and 47) and the International Labour Organization’s (ILO) Declaration on Fundamental Principles and Rights at Work 1998. All the informal workers of India must be entitled to freedom of association and the effective recognition of the right to collective bargaining, the elimination of forced or compulsory labour, the abolition of child labour and the elimination of discrimination in respect of employment and occupation. On behalf of all working people of south India we put forth the following demands to the union and state governments:

**Labour Law and Policy Reforms**

* We demand that the government restore and strengthen democratic consultation processes on labour law reforms, and labour administration in general, to align all new laws with the concerns of the working people of India.
* We stand in solidarity with Indian trade unions demanding the repeal of anti-worker provisions of the new labour laws.
* We note with concern the continuing informalisation of much of what was once a formal sector, the restrictions on unionising, and lack of access to the justice system.
* We insist that the government guarantee the protection and enlargement of labour rights under the new labour laws with time-bound progress towards formalization of the informal sector.
* The restriction on the number of workers should not be a criterion for access to entitlement.
* We insist that all new laws be made to guarantee universality and equity of social protection to all workers.
* We demand that the provision for a clear and accountable institutional arrangement guaranteeing a gender-equitable, universal social protection consisting of health and medical care coverage; pension; provident fund and gratuity; sickness, injury, disability and death compensations; maternity benefit; unemployment allowance; social housing benefit; and food security.
* The governments must additionally strengthen and universalize the Integrated Child Development Services to promote equal opportunity for women to participate in the labour market.
* We demand a guarantee of income and employment for urban workers on the lines of Kerala’s Ayyankali Urban Employment Scheme that has been running for almost a decade.
* In the interest of transparency, accountability and time-bound implementation, we demand that the concerned governments set up robust tripartite facilitation and monitoring mechanisms with due representation of informal workers.

**Comprehensive Social Health Protection**

* The country already has a comprehensive social health protection programme in the Employees’ State Insurance Scheme (ESIS). We demand that the ESIS be extended to all workers without exclusionary eligibility thresholds, in a time-bound manner.
* We emphasise that the benefit package for informal workers is at par with that for the organised sector workers. Further, as is in the ESI Act 1948, social health protection be recognized as a legally protected right of all workers irrespective of the nature of their employment or livelihood.
* The governments or the concerned industry must pay the employer’s contribution where individual employers cannot be identified.
* **We make it clear that expansion of coverage must not imply any erosion whatsoever, of benefits and entitlements of existing or new beneficiaries.**
* **New risk pools and cross-subsidies should be designed through democratic consultations with all stakeholders.**

**Housing for All**

* We demand that all concerned governments revise their land allotment and housing policies to accommodate the basic need for affordable and dignified housing of millions of workers, including migrants. The government should provide the working population with all basic amenities such as water, electricity, transport and childcare support.
* Special attention must be paid to those who cannot afford to pay for housing or are currently homeless. Such groups should be provided free housing.
* All housing policies must provide for protective spaces and shelters for victims of violence, especially women, children and rescued survivors of trafficking and bonded labour.
* All concerned governments must desist from forced evictions and enforced displacements.
* The concerned governments must also revisit and formulate its regulatory policies of the rental housing market, keeping in mind the increasingly temporary and mobile nature of jobs even for middle- and higher-income workers in India.

**Equitable Access and Protection in the Labour Market**

* We demand full recognition of women’s unpaid labour and in turn, their due protection both within and outside the household.
* We specifically commit ourselves to ensuring effective implementation of the Sexual Harassment of Women at Workplace (Prevention, Prohibition and Redressal) Act, 2013, and the Equal Remuneration Act 1976.
* We commit ourselves to ending all forms of discrimination and violence in the labour market and Indian society against all marginalized identities and social groups.
* We commit to mobilise all available legal instruments (such as the Scheduled Caste and Scheduled Tribe (Prevention of Atrocities) Act, 1989) and social-political platforms.
* We commit to strive for eradication of all belief systems that condone and propagate inequity and forced labour; and obstruct the march towards equal opportunity and collective prosperity.
* **We demand that the government must meet the longstanding demand of a right to work in urban areas which is critical for the recognition of myriad livelihood and economic activities of the informal workers, as well as their claim to equal citizenship entitlements in urban areas.**

**Decentralization and Financing of Labour Welfare**

* We urge both the union and state governments to work together towards resetting the developmental priorities on a human-centred sustainable recovery from the devastating pandemic.
* The constituents of the WPC are critical contributors in the process of national development. In fulfilling this role, we offer our complete cooperation to all levels of the government in re-building the society and the economy.
* At the same time, we reiterate our primary accountability to our constituents and their constitutional rights, offering no compromise on this front.

**Agriculture and Farm Policy Reforms**

* We demand that the government ensures that the farmers and peasants actively participate in the formation of all public policies for agriculture.
* We demand that the government uses all its policy instruments of agriculture and farm policy--ensuring profitable farm prices, advance purchase of the food production of all farmers and peasants, adequate rural credit, rural insurance, technological assistance, storage -- to encourage and train farmers and support the organic production of healthy food.
* We demand that the concerned governments reorganize and restructure the agricultural research -- directed primarily to organic farming for food production and to eliminate pesticides and genetic modifications in balance with nature and in line with the needs of family farming.
* We demand that the governments ensures technical assistance, the transfer of technologies, support and rural extensions are guided by the principles of the democratization of skills, favouring exchanges and encouragement for the creativity of the peasants and farmers.

**Land**

* We demand that access to land, to natural resources and to the means of production in agriculture should be democratized for all. Ensure that all agricultural workers have the means to have access to land to live and/or work.
* We demand that the government should ensure that the democratization of the use, possession and ownership of land and of natural resources is tied to the interests and social, economic, cultural and political needs of the farming population specifically and the whole population generally.
* We demand that the government should ensure that all the indigenous communities, forest dwellers, nomadic tribes, fisher folks, coastal and traditional communities have the means and access to possess and use the land and water.
* We demand the return of all Panchami lands that were illegally occupied by other communities and businesses and reallocate them to land redistribution among SC/ST.
* We demand that the government should consult all the families affected by public works, so the projects have minimal social and environmental impact. And if there is need for the public work, families must have the right to receive land in the same conditions in which they lived and fair compensation for losses and damages for their work and improvements to the buildings.
* We demand that the government should ensure that mining projects and coastal development projects by businesses will not be allowed in human settlement areas. The minerals and ocean resources should be utilized in a sustainable way to benefit the community and all the people.

**Water**

* Water is a natural resource and should be used to benefit humanity. We demand that the government should ensure the possession and use of water be subordinated to the interests and needs of the whole population.
* We oppose water becoming a commodity because it cannot be a private property. Access to water should be guaranteed for everyone. All the water reservoirs, dams, ponds and even underground aquifers should be in the public domain.

**Farm Production**

* We uphold the principle of food sovereignty by prioritizing the production of healthy food for the people, free of synthetic pesticides and GMO seeds.
* We stand for production and sales on a cooperative basis such as traditional forms of community organizations, associations, cooperatives, public enterprises and social enterprises.
* We demand that the government develop programs of energy independence in all the rural communities of the country based on renewable alternative sources with non-food plants, solar, water and wind.
* We demand that through the public supply company, the government must ensure the purchase of all the food products from family farms.

**Technological Matrix**

* We demand that the government provide credits, subsidized financing, research and technological training focused on organic farm production with the incentive to adopt techniques that increase productivity of labour and land in balance with nature.
* We demand that the government provide incentives and machinery, equipment, and appropriate agricultural tools for the needs and welfare of peasants and rural workers on the basis of regional realities and environmental preservation.
* We oppose intellectual private property and patents on seeds, animals, biodiversity, or production systems.
* To uphold the principle of food sovereignty and to ensure that farmers and peasants can produce foods that are part of Indian culture, the government must provide programs for the production, multiplication, storage and distribution of native and organic seeds.
* We demand the government to develop a national program of reforestation with native and fruit trees and forest management in settlement areas, farm areas, areas degraded by agribusiness and in areas controlled by forest dwellers, coastal communities, indigenous peoples and traditional communities.
* We demand the government to facilitate, promote, and establish public enterprises and farmers cooperatives to produce organic inputs that can be stored and distributed to all farmers.
* We demand the government to ensure that there is no production and sale of agrotoxins and GMO seeds in India.
* We demand the government to set up facilities to transform organic waste from cities into organic fertilizer and distribute it free to all farmers.

**Other Specific Demands**

* Relief fund and relief articles must be distributed to the permanent and temporary sanitary workers, no matter whether they are registered with the labour welfare board or not.
* Rs. 25,00000 must be distributed to the families of sanitary workers who died when they worked as the frontline during the pandemic. Their wards must be given government employment.
* We demand the withdrawal of all legal instruments that denies the rights of workers and the trade unions or dismantles the welfare boards of informal workers
* We demand that the state governments should allocate 1% of the total state GDP and the union government should allocate 3% of the national GDP to various welfare boards of informal workers. This must be notified in the financial reports of both the governments.
* Registration and renewal of memberships of informal workers with various labour welfare boards must be carried out through trade unions. Warranting community certificate and verification certificate by VAO must be avoided for simplifying the process of online registration.
* Medical care through ESI and financial assistances must be ensured to all of the informal workers: maternity benefit Rs.30,000, compensation of Rs.5 lakh to accident deaths, compensation of 1 lakh to natural deaths, Rs. 25,000 as marriage assistance and financial to the children of informal workers studying from 1st standard to higher level.
* Houses must be built on the free spaces and the lands taken in possession of the government and distributed freely to the homeless informal workers.
* Alternative houses must be provided to the informal workers, who have been evicted in big cities, within the city limits so as to ensure their livelihood resources.
* Registration and renewal of memberships with labour welfare boards, issuing family cards and childcare to the children must be ensured to the informal workers who are migrating.
* Separate ministries should be formed in all states to take care of migrant workers. Insurance must be guaranteed to all of the workers who migrate to other countries. Special trainings on the preferred jobs must be imparted to the migrant workers before they leave the domestic country. Government should take steps to rehabilitate the returnees.
* Each year, a district-level study must be made on the bonded labourers. They must be liberated from serfdom by compensating their debts and lands, shelters and education to the children must be provided to them as a measure of rehabilitation.
* Compulsory education must be given to all children up to 18 years of age. Every year, a study on the incidence of child labour must be made properly at district level. Government should establish residential schools for the child labourers rescued.
* Considering the present economic crisis and the ever mounting price rise, the government should fix Rs. 18,000 as the minimum monthly salary for an informal worker and implement it legally.
* Worksite safety and interim relief during the rainy season, which the construction workers enjoy, must be ensured to all informal workers. In all occupations, 70% of job opportunities must be ensured to the native people.
* India should ratify the ILO C 189
* Clay must be given at free of cost to the potters. The government should distribute earthen hearth to people as agricultural festival gifts.
* Free housing, electricity and professional equipments must be issued to hairdressers.
* A separate ministry must be formed for the welfare of salt pan workers. Potable water, rest rooms, crutches and medical care centers must be ensured for them at the worksites. Gumboots, gloves and goggles must be provided to them. Legal minimum wage, interim relief of Rs 5000 during rainy season and housing facilities must also be ensured to them. Salt pans of the state government and the union government must be given to the workers on lease for enhancing their economic development.
* Sanitary workers who work on contract basis in town panchayats, municipalities and corporations must be made permanent workers and regular salary must be paid to them. Sanitary workers who work on contract basis in village panchayats must be made permanent workers and Rs. 18,000 must be paid to them.
* The state government should enact a special Act for the welfare of domestic workers.
* The projects related to the Blue Economy and Sagharmala are detrimental to the coastal ecology, fish population and the livelihood of fisher-folk. Government should withdraw such projects immediately and ensure a safe coastal environment and the assured livelihood of fisher-folk.
* MNREGS must be extended also to urban areas and 200 days of man days must be guaranteed.
* The government should give up privatizing the service sectors like railway, electricity, transport, health and education. It should also give up privatizing the public sectors, which run profitably.
* For safeguarding women from any form of violence at the work places, the government should ensure the functioning of Internal Complaint Committees textile and garment industries in accordance with the Prevention of Sexual Offences Act-2013.

##### We are the united front of the historically marginalized, but inevitable future of this country – _the working people_. The women, men, trans-people, Adivasis, Dalits, religious minorities and all people in the WPC resolve in unison, to build a united collective, based on the principles of equity and non-discrimination, committed to our goals stated in this resolution. On this day, the WPC stands as a collective voice of informal workers in this country who are demanding social justice and dignity. We will persevere with unflinching commitment to the ideals of this _Sovereign, Socialist, Secular, Democratic, Republic_. The WPC calls upon the union government, the state governments, industry, employers and all the stakeholders in India’s journey into a just and prosperous future, to join forces with us for realizing the vision of this resolution!

##### **_VOICE AND ASSERT FOR IDENTITY, DIGNITY AND SOCIAL PROTECTION FOR ALL WORKERS! WORKING PEOPLES’ UNITY LONG LIVE!_**