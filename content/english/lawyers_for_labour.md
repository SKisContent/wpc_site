---
title: Lawyers for Labour
description: WPC has taken up an initiative to bring together lawyers working on labour
  issues, who can help WPC constituencies and work of the India Labourline, Labour
  Axis and campaign groups.
bg_image: "/images/w.jpg"
related_tag: lawyers_for_labour
image: "/images/whatsapp-image-2021-09-27-at-12-29-59.jpeg"
facts:
  enable: false
  fact_item: []
menu:
  main:
    URL: lawyers_for_labour
    weight: 11
    parent: home

---
**![](/images/whatsapp-image-2021-09-27-at-12-29-59.jpeg)**

#### **Lawyers for Labour:**

##### **Building a Lawyer’s Solidarity Network to support Labour**

***

During both waves of the Covid-19 pandemic, migrant workers were left literally and figuratively stranded in the midst of an unprecedented labour crisis. Not only were workers unable to access benefits under welfare schemes announced by State and Central Governments for the alleged welfare of workers but they were also kept ignorant about what and how they could claim their rights under the various laws.

Despite working for contractors and employers for years, Migrant workers were not aware that their contractors/employers are required to provide housing, travelling allowance for migrant workers to return home etc under the Inter-State Migrant Workers Act, 1971. A large number of workers were not paid their wages and were not aware how and from whom they could claim their wages. Many workers like street vendors, domestic workers, transport workers, health workers, construction workers, App-based workers and other unorganised workers and self-employed workers were left high and dry as they were thrown out of their jobs and were unable to claim their wages and other benefits under the Unorganised Workers Social Security Act, 2008 The Building And Other Construction Workers (Regulation Of Employment And Conditions Of Service) Act, 1996, Street Vendors (Protection of Livelihood and Regulation of Street Vending) Act, 2014 and other labour laws.

Labour laws enacted post Independence have been quietly flouted over the past two decades, and the denial of the fundamental rights of workers such as the right to assemble and form unions, right to life with human dignity etc. enshrined in the Constitution has become glaringly evident. The recent attempts of the government in power to recast the entire body of labour legislation in the form of labour codes have created further confusion and distress. But the forward march of the denial of rights has not abated.

A need was therefore felt to form a collective or network of labour lawyers who would be immediately available to help workers by approaching the Supreme Court, High Courts, Industrial/Labour Courts and Magistrate Courts as may be necessary to help anybody of workers facing legal difficulties.

This initiative is being taken with the help and support of the Working Peoples Charter (WPC) which is a broad network of organizations working on issues related to informal labour in particular, and labour in general. It is an independent entity not affiliated with any organization, federation or political party. WPC has chapters in various states in India and includes a large number of constituencies amongst its members. The Lawyers Solidarity Network will provide help to the work of WPC which will, in turn, help in disseminating information regarding success stories, and explaining the laws to workers in simple, clear language. The lawyers will work closely with the state chapters of the WPC and help to strengthen the reach and effectiveness of the work of the WPC network.

Lawyers for Labour will maintain a repository of government circulars and judgments that would prevent workers from being mistreated and were necessary to seek legal recourse for any issues being faced by them. It will create a space for activists, litigants and advocates to share ideas, experiences, and cases and get support and solidarity in the work they do at all levels of labour administration and courts. It will share knowledge and link various legal battles in various states.

***

Adv. Gayatri Singh, Meena Menon, Chandan Kumar

***

For more details, contact: Chinmayi Naik. Email: [chinmayi.naik@indialabourline.org](chinmayi.naik@indialabourline.org)